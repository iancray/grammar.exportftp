﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DocuStream.Grammar.ExportFTP.General
{
    public class DateTimeTool
    {
        public DateTime Today    { get => DateTime.Now; }
        public DateTime UTCToday { get => DateTime.UtcNow; }

        Dictionary<string, DateTime> _dates = new Dictionary<string, DateTime>();

        public DateTime this[string item]
        {
            get
            {
                if (this._dates.ContainsKey(item) == false)
                    this._dates[item] = DateTime.Now;

                return _dates[item];
            }
            set
            {
                this._dates[item] = value;
            }
        }
    }
}
