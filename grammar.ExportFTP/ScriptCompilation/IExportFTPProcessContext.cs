﻿namespace DocuStream.Grammar.ExportFTP.ScriptCompilation
{
    using DocuStream.ProcessingFramework.Application;

    // the interface type for which an instance will be constructed by the CSharpScriptVisitor.
    public abstract class IExportFTPProcessContext : BaseProcessContext
    {
        public override int ReturnBatchStatusValue { get; set; }
        public abstract void Transform();
        public abstract bool CheckAllSftpConnections();
    }
}
