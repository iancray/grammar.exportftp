﻿using System;
using System.Collections.Generic;
using System.Linq;
using DocuStream.ProcessingFramework.Reporting;

namespace DocuStream.Grammar.ExportFTP.ScriptCompilation
{
    public class ActionMessageListener : IObserver<Message>
    {
        private Action _action;

        public ActionMessageListener(Action action)
        {
            this._action = action;
        }

        public static ActionMessageListener GetActionMessageListener<T>(T a, Action<T> f) => new ActionMessageListener(a.Bind(f));
        public static ActionMessageListener GetActionMessageListener(Action f) => new ActionMessageListener(f);
        public void OnCompleted()
        {
            throw new NotImplementedException();
        }

        /*
         *  this._private_helper_methods.Add(
                $"private void {transform_action_expression_method_name}(ISet<EFFile> files)\n" +
                $"{{\n" +
                        $"{method_body}".Indent() + "\n" +
                $"}}\n"
            );
         *  
         
            // 1. if ( /* has not been assigned up  ) assign up on frequency-x
            // 2. raise frequency-x
         */

        public void OnError(Exception error)
        {
            throw new NotImplementedException();
        }

        public void OnNext(Message value)
        {
            this._action();
        }
    }

    public static class ActionBindExte
    {
        public static Action Bind<T>(this T a, Action<T> f)
        {
            return () => f(a);
        }

        public static Action<T2> Bind<T1, T2>(this T1 a, Action<T1, T2> f)
        {
            return _ => f(a, _);
        }

        public static Action<T2> Bind<T1, T2>(this Action<T1, T2> f, T1 a)
        {
            return _ => f(a, _);
        }

        public static Action Bind<T>(this Action<T> f, T a)
        {
            return () => f(a);
        }
    }
}