﻿I'll be completely honest, there's not much use for the classes specd out in this directory.

I thought I would want an object per component.txt file, but it really turned out that C# anonymous objects were sufficient, and frankly much less burdensome.

-- Ian Ray, March 29, 2018