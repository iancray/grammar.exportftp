﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocuStream.Grammar.ExportFTP.Deprecated.RunTimeConstruction.General
{
    public static class FileInfoExtension
    {
        public static string SimpleName(this FileInfo file)
        {
            return file.Name.Substring(0, file.Name.LastIndexOf('.'));
        }
    }
}
