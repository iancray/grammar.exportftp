﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DocuStream.Grammar.ExportFTP.General;
//using DocuStream.ProcessingFramework.Application;

namespace processing.ExportFTP
{
    //using EFKitToNamedEFFilesSet = Func<EFKit, IEnumerable<(string name, IEnumerable<EFFile> files)>>;
    //using EFFiles = IEnumerable<EFFile>;
    //using Files = IEnumerable<FileInfo>;
    //using NamedEFFileSet = IEnumerable<(string name, IEnumerable<EFFile> files)>;
    //using EFKitToNamedEFFilesSetToNamedEFFilesSet = Func<EFKit, Func<IEnumerable<(string name, IEnumerable<EFFile> files)>, IEnumerable<(string name, IEnumerable<EFFile> files)>>>;
    //using EFKitToNamedEFFilesSetToBool = Func<EFKit, Func<IEnumerable<(string name, IEnumerable<EFFile> files)>, bool>>;
    //public class ExportFTPContext : BaseProcessContext, IObserver<(string name, IEnumerable<EFFile> files)>
    //{
    //    private EFKitToNamedEFFilesSetToNamedEFFilesSet _transform;
    //    private EFKitToNamedEFFilesSet _named_file_generation;
    //    private EFKitToNamedEFFilesSetToBool _data_validation;
    //    private EFKit _efKit;
    //    private IEnumerable<((string name, EFFiles files) named_files, Func<Files, bool> data_validation)> _resultsDictionary;
    //    private NamedEFFileSet _neffs;

    //    public override int ReturnBatchStatusValue { get; set; }

    //    public ExportFTPContext(EFKitToNamedEFFilesSet named_file_generation, EFKitToNamedEFFilesSetToBool data_validation, EFKitToNamedEFFilesSetToNamedEFFilesSet transform)
    //    {
    //        this._transform = transform;
    //        this._named_file_generation = named_file_generation;
    //        this._data_validation = data_validation;
    //        //this._efKit                    = new EFKit();
    //        //this._efKit.Subscribe(this);
    //    }

    //    public override void InitializeContext()
    //    {
    //        this._efKit.State = this._state;
    //    }

    //    public override void ImageProcessing(Image i)
    //    {
    //        this._efKit.Image = i;
    //        this._neffs = this._named_file_generation(this._efKit);
    //        //.Select(t => (named_files: t.named_file_generation(this._efKit), data_validation: t.data_validation(this._efKit)));
    //    }

    //    public override void BatchProcessing(Batch b)
    //    {
    //        //var allFilesCheckOutAsRequired = this._resultsDictionary.Aggregate(
    //        //    seed: true,
    //        //    func: (acc, next) => acc && next.data_validation(next.named_files.files));
    //        var allFilesCheckOutAsRequired = this._data_validation (this._efKit) (this._neffs);

    //        if (allFilesCheckOutAsRequired)
    //        {
    //            this.ReturnBatchStatusValue = int.Parse(this.State["TransformBatchStatusValue"]);
    //        }
    //        else
    //        {
    //            this.ReturnBatchStatusValue = int.Parse(this.State["ErrorBatchStatusValue"]);
    //            return;
    //        }
    //    }

    //    public void OnError(Exception error)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public void OnCompleted()
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public void OnNext((string name, EFFiles files) value)
    //    {
    //        if (this._neffs.Any(n => value.name == n.name))
    //        {
    //            this._neffs = this._neffs.Select(n =>
    //            {
    //                if (n.name == value.name)
    //                    return (value.name, value.files.Union(n.files));
    //                else
    //                    return n;

    //            }).ToList();
    //        }
    //        else
    //        {
    //            this._neffs = this._neffs.Union(new [] { value }).ToList();
    //        }
    //    }

    //    public void Transform()
    //    {
    //        this._neffs = this._transform(this._efKit)(this._neffs);
    //    }
    //}
}
