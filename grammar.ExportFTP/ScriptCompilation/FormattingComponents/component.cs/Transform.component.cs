﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocuStream.Grammar.ExportFTP.ScriptCompilation.FormattingComponents.component.cs
{
    public class TransformComponent
    {
        public string TransformFunctionName { get; set; }
        public string TransformKey          { get; set; }
        public string WhereClause           { get; set; }
        public List<string> CleanUpChannel        { get; set; }
    }


}
