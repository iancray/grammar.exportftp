﻿using System;
using DocuStream.Grammar.ExportFTP.ScriptCompilation;
using DocuStream.ProcessingFramework.Application;
using DocuStream.ProcessingFramework.Data;
using DocuStream.ProcessingFramework.Reporting;
using DocuStream.ProcessingFramework.Utilities;

namespace Forms.Grammar.ExportFTP
{
    public class CustomProcessContextAdapter : ProcessContextAdapter<IDocuStreamProcessContext>
    {
        Func<bool> _preCheckCall;
        public CustomProcessContextAdapter(string moduleName, IExportFTPProcessContext context, IReporter reporter, IBatchEntryProvider batchEntryProvider, IProcessContextConsumer<IDocuStreamProcessContext> consumer) 
        : base(moduleName, context, reporter, consumer)
        {
            this._consumer = consumer;
            this._preCheckCall = context.CheckAllSftpConnections;
        }

        public override void Start()
        {
            // allows an IExportFTPProcessContext to 
            if (_preCheckCall() == false)
            {
                return;
            }

            while (this.ShouldContinue)
            {
                _context.InitializeContext();
                while (Main().ProcessedEntity){}
            }
        }

        public IProcessableResult Main()
        {
            try
            {
                return _consumer.Consume(_context);    
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}