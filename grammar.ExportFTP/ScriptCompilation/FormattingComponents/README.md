﻿This directory is to for the sake of keeping our auto-compiled code organized and legibly formatted.
We use the SmartFormatter.NET library in order to keep all variable components neatly registered and configurable.

In order to create a new component, simply create a new text file and use the file convension "<name-of-component>.component.txt",
and then click properties on that component, and for Build Action, set it as an embedded resource.

Then in the CSharpScriptVisitor, use FormatComponent("<name-of-component>", new { /* properties for fmt component */ }); call to resolve the output c# code.

--Ian Ray