﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocuStream.Grammar.ExportFTP.General;

namespace DocuStream.Grammar.ExportFTP.ScriptCompilation
{
    public class DualKeyDictionary<T> : Dictionary<string, T>
    {
        public IEnumerable<T> LookupMatchesFrom(string a, string b)
        {
            if (a == null && b == null)
                return this.Select(_ => _.Value);

            var lookup = GetLookUp();
            return lookup.Where(_ =>
            {
                if (
                    (_.Key._a == a || _.Key._a == null)
                    &&
                    (_.Key._b == b || _.Key._b == null)
                ) return true;

                return false;
            }).Select(_ => _.Value);
        }

        public IEnumerable<T> LookupMatchesTo(string a, string b)
        {
            var lookup = GetLookUp();
            return lookup.Where(_ =>
            {
                if (_.Key._a == "" && _.Key._b == "")
                    return true;

                if (_.Key._a == "" && _.Key._b == b)
                    return true;

                if (_.Key._a == a && _.Key._b == "")
                    return true;

                if (_.Key._a == a && _.Key._b == b)
                    return true;

                return false;
            }).Select(_ => _.Value).ToList();
        }

        public Dictionary<(string _a, string _b), T> GetLookUp()
        {
            return this.Select(_ => _).ToDictionary(
                        _ => _.Key.Split(':').ToList().Into(
                            i => 
                                (
                                    i.Try(j => j[0], ""), 
                                    i.Try(j => j[1], "")
                                )
                        ), 
                        _ => _.Value
            );
        }
    }
}
