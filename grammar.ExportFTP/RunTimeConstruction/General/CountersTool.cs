﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;

namespace DocuStream.Grammar.ExportFTP.General
{
    // A file-system based memoized counter, which resets daily.
    public class DailyCounter
    {
        public DailyCounter(string path)
        {
            uiats_path = path;
        }

        static void SetCounterValue(string filePath, string name, int value)
        {
            List<NamedUploadIndexAndTimeStamp> uiats_arr = null;

            if (File.Exists(filePath))
            {
                uiats_arr = JsonConvert.DeserializeObject<List<NamedUploadIndexAndTimeStamp>>(File.ReadAllText(filePath));
                if (!uiats_arr.Any(u => u.Name == name))
                {
                    uiats_arr.Add(new NamedUploadIndexAndTimeStamp
                    {
                        Name = name,
                        Index = value,
                        TimeStamp = DateTime.UtcNow
                    });
                }
                else
                {
                    uiats_arr.First(u => u.Name == name).Index = value;
                }
                File.WriteAllText(filePath, JsonConvert.SerializeObject(uiats_arr));
            }
            else
            {
                uiats_arr = new List<NamedUploadIndexAndTimeStamp>
                {
                    new NamedUploadIndexAndTimeStamp
                    {
                        Name = name,
                        Index = value,
                        TimeStamp = DateTime.UtcNow
                    }
                };
                var fi = new FileInfo(filePath);
                if (!Directory.Exists(fi.DirectoryName))
                    Directory.CreateDirectory(fi.DirectoryName);
                File.WriteAllText(filePath, JsonConvert.SerializeObject(uiats_arr));
            }
        }

        static int GetCounterValue(string filePath, string name)
        {
            List<NamedUploadIndexAndTimeStamp> uiats_arr = null;
            NamedUploadIndexAndTimeStamp uiats = null;

            if (File.Exists(filePath))
            {
                uiats_arr = JsonConvert.DeserializeObject<List<NamedUploadIndexAndTimeStamp>>(File.ReadAllText(filePath));
                uiats = uiats_arr.FirstOrDefault(u => u.Name == name);
                if (uiats == null)
                {

                    // Not an array but still feels relevant :) http://rebrn.com/re/array-indexing-round-3311424/
                    uiats = new NamedUploadIndexAndTimeStamp
                    {
                        Name = name,
                        Index =  1,
                        TimeStamp = DateTime.UtcNow
                    };
                    uiats_arr.Add(uiats);
                }
                else if (uiats.TimeStamp.ToLocalTime().DayOfYear < DateTime.Now.DayOfYear)
                {
                    uiats.Index = 1;
                    uiats.TimeStamp = DateTime.UtcNow;
                }
                File.WriteAllText(filePath, JsonConvert.SerializeObject(uiats_arr));
            }
            else
            {
                uiats_arr = new List<NamedUploadIndexAndTimeStamp>
                {
                    new NamedUploadIndexAndTimeStamp
                    {
                        Name = name,
                        Index = 1,
                        TimeStamp = DateTime.UtcNow
                    }
                };
                var fi = new FileInfo(filePath);
                if (!Directory.Exists(fi.DirectoryName))
                    Directory.CreateDirectory(fi.DirectoryName);
                File.WriteAllText(filePath, JsonConvert.SerializeObject(uiats_arr));
                uiats = uiats_arr[0];
            }

            return uiats.Index;
        }

        string uiats_path = null;
        public int this[string item]
        {
            // because i'm lazy at this point.
            get => GetCounterValue(uiats_path, item);
            set => SetCounterValue(uiats_path, item, value);
        }

        class NamedUploadIndexAndTimeStamp
        {
            [JsonProperty]
            public string Name { get; set; }
            [JsonProperty]
            public int Index { get; set; }
            [JsonProperty]
            public DateTime TimeStamp { get; set; }
        }
    }


    public class CounterTool
    {
        Dictionary<string, int> _counters = new Dictionary<string, int>();
        public DailyCounter DailyCounter { get; set; }

        public CounterTool(string path)
        {
            DailyCounter = new DailyCounter(path);
        }

        public int this[string item]
        {
            get
            {
                if (this._counters.ContainsKey(item) == false)
                    this._counters[item] = 0;

                return this._counters[item];
            }
            set
            {
                this._counters[item] = value;
            }
        }
    }
}