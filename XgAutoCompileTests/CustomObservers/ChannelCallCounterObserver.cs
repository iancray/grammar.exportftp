﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocuStream.ProcessingFramework.Reporting;

namespace XgAutoCompileTests.CustomObservers
{
    class SimpleChannelCallCounterObserver : IObserver<Message>
    {
        public int Count { get; set; }

        public void OnCompleted()
        {
            throw new NotImplementedException();
        }

        public void OnError(Exception error)
        {
            throw new NotImplementedException();
        }

        public void OnNext(Message value)
        {
            Count++;
        }
    }
}
