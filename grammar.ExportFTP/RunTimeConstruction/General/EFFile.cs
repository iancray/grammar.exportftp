﻿using System;
using System.Collections.Generic;
using System.IO;
using DocuStream.ProcessingFramework.Application;

namespace DocuStream.Grammar.ExportFTP.General
{
    public class EFFile
    {
        public FileInfo File { get; set; }
        public Image Image { get; set; }

        public override int GetHashCode()
        {
            return File.FullName.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj is EFFile eff)
            {
                return eff.File.FullName == File.FullName;
            }

            (var a, var b) = "a:b".Split(':').Into(_ => (_.Try(x => x[0], null), _.Try(x => x[1], null)));

            return false;
        }
    }

    public static class Some
    {
        public static TOut Into<TIn, TOut>(this IList<TIn> enumerable, Func<IList<TIn>, TOut> transform)
         => transform(enumerable);   
        
        public static TOut Try<TIn, TOut>(this TIn @in, Func<TIn, TOut> transform, TOut @default)
        {
            try
            {
                return transform(@in);
            }
            catch
            {
                return @default;
            }
        }
    }
}
