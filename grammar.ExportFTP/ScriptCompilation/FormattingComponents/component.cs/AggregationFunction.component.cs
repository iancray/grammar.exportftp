﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocuStream.Grammar.ExportFTP.ScriptCompilation.FormattingComponents.component.cs
{
    public class AggregateFunctionComponent
    {
        public string NameSource { get; set; }
        public string AccumulationFunction { get; set; }
        public bool DeleteFiles { get; set; }
    }
}
