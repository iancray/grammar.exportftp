﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocuStream.ProcessingFramework.Application;
using DocuStream.ProcessingFramework.Data;
using DocuStream.ProcessingFramework.Reporting;
using DocuStream.ProcessingFramework.Utilities;

namespace visit
{
    public class CustomWorkflow : Workflow
    {
        Action _call;
        public CustomWorkflow(Action call, IEnvironmentState state, IBatchEntryProvider batchProvider, IProcessContext context, string moduleName, IReporter reporter, EntityConstructorFactory constructory = null) 
        : base(state, batchProvider, context, moduleName, reporter, constructory)
        {
            this._call = call;
        }

        public override void NoRunnableBatchFound()
        {
            this._call();
        }
    }
}
