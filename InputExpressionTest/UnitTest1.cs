﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Antlr4.Runtime;
using DocuStream.Grammar.ExportFTP;
using DocuStream.Grammar.ExportFTP.General;
using DocuStream.Grammar.ExportFTP.Input;
using DocuStream.Grammar.ExportFTP.Transform;
using DocuStream.Processing.ExportFTP;
using DocuStream.ProcessingFramework.Application;
using DocuStream.ProcessingFramework.Data;
using DocuStream.ProcessingFramework.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using processing.ExportFTP;

namespace InputExpressionTest
{
    using EFKitToFilesToFiles = Func<EFKit, Func<IEnumerable<FileInfo>, IEnumerable<FileInfo>>>;
    using EFKitToFilesToBool = Func<EFKit, Func<IEnumerable<FileInfo>, bool>>;

    [TestClass]
    public class UnitTest1
    {
        ExportFTPParser GetParserForFile(string fileName)
        {
            var _inputStream = new AntlrInputStream(File.Open(fileName, FileMode.Open));
            var _parser = new ExportFTPParser(new CommonTokenStream(new ExportFTPLexer(_inputStream)));
            return _parser;
        }

        BeatlesVisitor beatlesVisitor;
        InputGetFilesDefinitionVisitor getFilesDefinitionVisitor;
        private InputVarWhereExpressionVisitor inputVarWhereExpressionVisitor;
        private AndFinallyCheckConditionVisitor andFinallyCheckConditionVisitor;
        private InputAndFinallyCheckExpressionVisitor inputAndFinallyCheckExpressionVisitor;
        InputExpressionVisitor inputExpressionVisitor;
        TransformActionOptionVisitor transformActionOptionVisitor;
        TransformActionVisitor transformActionVisitor;
        TransformActionChainVisitor transformActionChainVisitor;
        TransformWhereExpressionVisitor transformWhereExpressionVisitor;
        TransformGetFilesDefintionVisitor transformGetFilesDefintionVisitor;
        TransformExpressionVisitor transformExpressionVisitor;
        private CompileUnitVisitor compileUnitVisitor;
        InputCheckExpressionVisitor checkExpressionVisitor;
        WhereExpressionVisitor whereExpressionVisitor;
        private CheckConditionCompletionVisitor checkConditionCompletionVisitor;
        GeneralExpressionVisitor generalExpressionVisitor;
        ConfigCompiler configCompiler;
        private TransformVisitor transformVisitor;
        private IEnvironmentState ini;

        [TestInitialize]
        public void InitializeTestEnvironment()
        {
            ini = IniEnvironmentState.ReadIni("ExportFTP", "DocuStream");
            configCompiler                    = new ConfigCompiler                   ();
            generalExpressionVisitor          = new GeneralExpressionVisitor         (configCompiler);
            transformWhereExpressionVisitor   = new TransformWhereExpressionVisitor  (generalExpressionVisitor);
            whereExpressionVisitor            = new WhereExpressionVisitor           (generalExpressionVisitor);
            checkConditionCompletionVisitor   = new CheckConditionCompletionVisitor(generalExpressionVisitor);
            checkExpressionVisitor            = new InputCheckExpressionVisitor      (checkConditionCompletionVisitor);
            beatlesVisitor                    = new BeatlesVisitor                   ();
            getFilesDefinitionVisitor         = new InputGetFilesDefinitionVisitor   (beatlesVisitor, checkExpressionVisitor, whereExpressionVisitor, generalExpressionVisitor);
            inputVarWhereExpressionVisitor = new InputVarWhereExpressionVisitor(transformWhereExpressionVisitor);
            andFinallyCheckConditionVisitor = new AndFinallyCheckConditionVisitor(checkConditionCompletionVisitor, inputVarWhereExpressionVisitor);
            inputAndFinallyCheckExpressionVisitor = new InputAndFinallyCheckExpressionVisitor(andFinallyCheckConditionVisitor);
            inputExpressionVisitor            = new InputExpressionVisitor           (getFilesDefinitionVisitor, inputAndFinallyCheckExpressionVisitor);
            transformActionOptionVisitor      = new TransformActionOptionVisitor     (generalExpressionVisitor);
            transformActionVisitor            = new TransformActionVisitor           (generalExpressionVisitor, transformActionOptionVisitor);
            transformActionChainVisitor       = new TransformActionChainVisitor      (transformActionVisitor);
            transformGetFilesDefintionVisitor = new TransformGetFilesDefintionVisitor(generalExpressionVisitor);
            transformExpressionVisitor        = new TransformExpressionVisitor       (beatlesVisitor, transformGetFilesDefintionVisitor, transformWhereExpressionVisitor, transformActionChainVisitor);
            transformVisitor                  = new TransformVisitor(transformExpressionVisitor);
            compileUnitVisitor                = new CompileUnitVisitor               (inputExpressionVisitor, transformVisitor);
        }

        [TestMethod]
        public void TestSimple_GeneralExpressionIsType_Task_MethodInfo()
        {
            var parser = GetParserForFile("GeneralExpression1.txt");

            Assert.AreEqual(typeof(Task<object>), generalExpressionVisitor.VisitGeneral_expression(parser.general_expression()).GetType());
        }
        
        [TestMethod]
        public void TestComplex_GeneralExpressionIsType_Task_MethodInfo()
        {
            var parser = GetParserForFile("GeneralExpression2.txt");

            Assert.AreEqual(typeof(Task<object>), generalExpressionVisitor.VisitGeneral_expression(parser.general_expression()).GetType());
        }

        [TestMethod]
        public void WhereExpression_SimpleGenExpr_IsSubclassOfImportantType()
        {
            var parser = GetParserForFile("WhereExpression1.txt");

            Assert.IsTrue(whereExpressionVisitor.VisitWhere_expression(parser.where_expression()).GetType().IsSubclassOf(typeof(Task<EFKitToFilesToFiles>)));
        }

        [TestMethod]
        public void WhereExpression_ComplexGenExpr_IsSubclassOfImportantType()
        {
            var parser = GetParserForFile("WhereExpression2.txt");

            Assert.IsTrue(whereExpressionVisitor.VisitWhere_expression(parser.where_expression()).GetType().IsSubclassOf(typeof(Task<EFKitToFilesToFiles>)));
        }

        [TestMethod]
        public void CheckCondition_SimpleGenExpr_IsSubclassOfImportantType()
        {
            var parser = GetParserForFile("CheckExpression1.txt");

            Type type = checkExpressionVisitor.VisitCheck_condition(parser.check_condition()).GetType();
            Assert.IsTrue(type.IsSubclassOf(typeof(Task<EFKitToFilesToBool>)));
        }

        [TestMethod]
        public void CheckCondition_ComplexGenExpr_IsSubclassOfImportantType()
        {
            var parser = GetParserForFile("CheckExpression2.txt");

            Type type = checkExpressionVisitor.VisitCheck_condition(parser.check_condition()).GetType();
            Assert.IsTrue(type.IsSubclassOf(typeof(Task<EFKitToFilesToBool>)));
        }
        
        [TestMethod]
        public void RocketShipTest_Demo()
        {
            var parser = GetParserForFile("FullExpressionAlphaZulu.txt");
            var item = this.compileUnitVisitor.VisitCompileUnit(parser.compileUnit());
            var transforms = item.transform;
            
            this.configCompiler.Compile();
            try
            {
                item.input.named_file_generation.Wait();
                item.input.data_validation.Wait();
                transforms.Wait();
            }
            catch
            {
                var x = 0;
            }
            var ctx = new ExportFTPContext(item.input.named_file_generation.Result, item.input.data_validation.Result, item.transform.Result);
            ExportFTPWorkflowVisitor customWorkflowVisitor 
                = new ExportFTPWorkflowVisitor(
                    new Workflow(
                            null, 
                        ini, 
                        BatchEntryProvisioning.GetBatchEntryProvider(ini, "ExportFTP", new string[] { }), 
                        ctx, 
                        new ErrLogger(ini["LogFilePath"], "ExportFTP"), 
                        "ExportFTP"
                    )
                );

            customWorkflowVisitor.Main();
        }
    }
}
