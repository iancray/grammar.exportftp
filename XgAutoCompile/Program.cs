﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Xml.Serialization;
using Antlr4.Runtime;
using DocuStream.Grammar.ExportFTP;
using DocuStream.Grammar.ExportFTP.General;
using DocuStream.Grammar.ExportFTP.General.Transform.Utilities;
using DocuStream.Grammar.ExportFTP.ScriptCompilation;
using DocuStream.ProcessingFramework.Application;
using DocuStream.ProcessingFramework.Data;
using DocuStream.ProcessingFramework.Reporting;
using DocuStream.ProcessingFramework.Utilities;
using Ionic.Zip;
using Ionic.Zlib;
using System.Linq;
using Microsoft.CSharp;
using Renci.SshNet;
using WinSCP;

namespace XgAutoCompile
{
    public class Program
    {
        // SubText3: Tools -> Build System -> New Build System...
        //
        //
        // { "cmd" : ["<path-to-XgAutoCompile.exe>", "$file"] }
        // save as.
        public static int Main(string[] args)
        {
            Console.WriteLine($"XgAutoCompile v{FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).ProductVersion} by Ian Ray");
            Console.WriteLine($"Auto-compiling file: {args[0]}");
            try
            {
                (var tok, var parser) = GetParserForFile(args[0]);
                var file = new FileInfo(args[0]);
                string executableName = file.Name.Split('.')[0];
                CSharpScriptVisitor cssv = new CSharpScriptVisitor();
                cssv.TokenStream = tok;
                ExportFTPParser.CompileUnitContext compileUnitContext = parser.compileUnit();
                var code = compileUnitContext.Accept(cssv);

                CSharpCodeProvider provider = new CSharpCodeProvider(new Dictionary<String, String> { { "CompilerVersion", "v4.0" } });
                CompilerParameters parameters = new CompilerParameters(
                    new[]
                    {
                        "System.dll",
                        "System.io.dll",
                        typeof(ZipFile).Assembly.Location,
                        typeof(CompressionLevel).Assembly.Location,
                        typeof(EFKit).Assembly.Location,
                        typeof(Image).Assembly.Location ,
                        typeof(IEnvironmentState).Assembly.Location,
                        typeof(IExportFTPProcessContext).Assembly.Location,
                        typeof(System.Threading.Thread).Assembly.Location,
                        typeof(IReporter).Assembly.Location,
                        typeof(IBatchEntry).Assembly.Location,
                        typeof(System.ValueTuple<string,string>).Assembly.Location,
                        typeof(GPG_DS).Assembly.Location,
                        typeof(EmailWriter).Assembly.Location,
                        typeof(DataRowCollection).Assembly.Location,
                        typeof(IXmlSerializable).Assembly.Location,
                        typeof(WinSCP.Session).Assembly.Location
                    },
                    Path.Combine(file.DirectoryName, $"ExportFTP.DynamicCompiled.{executableName}.DLL"),
                    true
                );
                parameters.CompilerOptions += "/optimize";
                parameters.ReferencedAssemblies.Add(@"mscorlib.dll");
                parameters.ReferencedAssemblies.Add(@"System.Core.dll");
                parameters.ReferencedAssemblies.Add(@"System.Runtime.dll");
                CompilerResults results = provider.CompileAssemblyFromSource(parameters, (string)code);
                string localPath = Path.Combine(file.DirectoryName, "ExportFTP.DynamicallyCompiled." + executableName + ".cs");
                if (results.Errors.Count > 0)
                {
                    var errors_message = "Auto-compilation because:\n";
                    foreach( var exception in results.Errors)
                    {
                        errors_message += exception + "\n";
                    }
                    Console.WriteLine(errors_message);
                }
                else
                {
                    Console.WriteLine("Auto-compilation successful.");
                    string basePath = results.TempFiles.BasePath;
                    var fileEnding = basePath.Split(new[] { "Temp\\" }, StringSplitOptions.None)[1] + ".0.cs";

                    if (Directory.Exists(Path.Combine(file.DirectoryName, "CompileResultsTemp")) == false)
                    {
                        Directory.CreateDirectory(Path.Combine(file.DirectoryName, "CompileResultsTemp"));
                    }
                    File.WriteAllText(Path.Combine(file.DirectoryName, "CompileResultsTemp", fileEnding), (string)code);
                }
            
                File.WriteAllText(localPath, (string)code);
            }
            catch(Exception e)
            {
                Console.WriteLine("Auto-compilation unsuccessful:");
                Console.WriteLine(e.Message);
                return 1;
            }
            return 0;
        }

        static (CommonTokenStream tok, ExportFTPParser parser) GetParserForFile(string fileName)
        {
            string input = File.ReadAllText(fileName);
            var _inputStream = new AntlrInputStream(input);
            ExportFTPLexer _lexer = new ExportFTPLexer(_inputStream);
            CommonTokenStream _tok = new CommonTokenStream(_lexer);
            var _parser = new ExportFTPParser(_tok);

            return (_tok, _parser);
        }
    }
}
