﻿namespace DocuStream.Grammar.ExportFTP.ScriptCompilation.FormattingComponents.component.cs
{
    public class ExportFTPProcessContextComponent
    {
        public string EventListeners                        { get; set; }
        public string InitializeContextMethod               { get; set; }
        public string TransformFunction                     { get; set; }
        public string ImageProcessing                       { get; set; }
        public string BatchProcessing                       { get; set; }
        public string AttemptSftpUpload                     { get; set; }
        public string PrivateHelperMethods                  { get; set; }
        public string SqlHelper                             { get; set; }
        public string DeleteOriginalFilesOption_many        { get; set; }
        public string DeleteOriginalFilesOption             { get; set; }
        public string IncludeToHashSetPublicStaticExtension { get; set; }
        public string RecursiveDirectoryNuke                { get; set; }
        public string PreCheckFunction                      { get; set; }
    }
}
