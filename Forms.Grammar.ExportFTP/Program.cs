﻿using System;
using System.Reflection;
using System.Windows.Forms;
using DocuStream.Grammar.ExportFTP.General;
using DocuStream.Grammar.ExportFTP.ScriptCompilation;
using DocuStream.ProcessingFramework.Application;
using DocuStream.ProcessingFramework.Data;
using DocuStream.ProcessingFramework.Reporting;
using DocuStream.ProcessingFramework.Utilities;
using System.IO;

namespace Forms.Grammar.ExportFTP
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            try
            {
                var expectedKeys = new string[] { };
                IniEnvironmentState state ;
                var arguments = DocuStreamCommandLineArguments.ParseCommandLine(args);
                arguments.RUN_ONCE = true; // export ftp is one of those programs that doesn't need to run multiple times.
                try
                {
                    state = IniEnvironmentState.ReadIni("ExportFTP", "DocuStream");
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, "",
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Error);

                    return;
                }
                var provider = new BatchEntryProviderFactory(state, "ExportFTP", new string[] { }).GetBatchEntryProvider();
                EFKit efk = new EFKit(state, provider);
                var reporter = new MultiChannelReporterFactory().CreateMultiChannelReporterWithLogger(state["LogFilePath"]);
                LogWriter logWriter = LogWriter.DefaultLogWriterFactory(state.Dated("LogFilePath"));
                reporter.Subscribe(logWriter, true, "LOG", "ERROR");
                Assembly assembly = null;
                try
                {
                    assembly= Assembly
                        .LoadFrom(state["TargetAssembly"]);
                }
                catch(Exception inner)
                {
                    throw new Exception("Failed to read or load assembly from `TargetAssembly` ini-property. It may help to recompile the .xg script using the latest version of the auto-compiler.", inner);
                }

                IExportFTPProcessContext procCtx = null;
                try
                {
                    var type = assembly.GetType("ExportFTP.ProcessContext.ExportFTPProcessContext");
                    var ctor = type
                        .GetConstructor(new Type[] { typeof(EFKit), typeof(IReporter) });

                    var invokation = ctor.Invoke(new object[] { efk, reporter });
                    procCtx = invokation as IExportFTPProcessContext;
                }
                catch
                {
                    throw new Exception("Failed to instantiate IExportFTPProcessContext from `TargetAssembly`. It may help to recompile the .xg script using the latest version of the auto-compiler.");
                }
                try
                {
                    var prog = new CustomProcessContextAdapter(
                        "ExportFTP",
                        procCtx,
                        reporter,
                        provider,
                        new CustomWorkflow(procCtx.Transform, state, new BatchEntryProviderFactory(state, "ExportFTP").GetBatchEntryProvider(), "ExportFTP", reporter)
                    );

                    //var contextProvider = new InvariantContextProvider(procCtx);
                    ProcessContextAdapter<IDocuStreamProcessContext> main = new ProcessContextAdapter<IDocuStreamProcessContext>("ExportFTP", procCtx, reporter, new Workflow(state, new BatchEntryProviderFactory(state, "ExportFTP").GetBatchEntryProvider(), "ExportFTP", reporter));
                    var config = new ControllerConfiguration("ExportFTP", arguments, state, main);
                    Application.EnableVisualStyles();
                    Application.SetCompatibleTextRenderingDefault(false);
                    Application.Run(new DSModelViewController(config));
                }
                catch
                {
                    throw new Exception("Failed to run the process specified in `TargetAssembly`. It may help to recompile the .xg script using the latest version of the auto-compiler.");
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Failed to initialize ExportFTP due to following exception: " + e.Message + "\nProgram will close after this message is closed.");
            }
        }
    }
}