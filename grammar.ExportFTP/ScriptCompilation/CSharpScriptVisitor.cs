﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Antlr4.Runtime;
using Antlr4.Runtime.Misc;
using DocuStream.Grammar.ExportFTP.General;
using DocuStream.Grammar.ExportFTP.ScriptCompilation.FormattingComponents.component.cs;
using SmartFormat;
using System.Data;

namespace DocuStream.Grammar.ExportFTP.ScriptCompilation
{
    public class CSharpScriptVisitor : ExportFTPParserBaseVisitor<object>
    {
        Type _type;
        int _general_expression_iterator;
        int _transform_action_counter;
        int _transform_action_sub_counter;
        int _transform_file_snapshot_counter;
        Stack<string> _action_files_out_stack;
        List<string> _private_helper_methods = new List<string>();
        List<string> _initialize_context_transform_reset_commands = new List<string>();
        List<string> _check_expressions = new List<string>();
        
        string _input_staging_key;
        private int email_channel_iterator;

        public ITokenStream TokenStream { get; set; }
        public CSharpScriptVisitor()
        {
            this._action_files_out_stack = new Stack<string>();
            this._cleanup_channel = new List<string>();
            _sftp_pre_check_options = new List<object>();
            InitFormatting();
        }

        public static void InitFormatting()
        {
            Smart.Default.Parser.UseAlternativeEscapeChar('\\');
        }

        public override object VisitCompileUnit([NotNull] ExportFTPParser.CompileUnitContext context)
        {
            if (context.events() != null)
            {
                context.events().Accept(this);
            }

            var image_processing = FormatComponent("ImageProcessing", new ImageProcessingComponent { Body = ((string)context.input().Accept(this)).Indent() });

            string if_criteria = "true";
            if (this._check_expressions.Count() > 0)
                if_criteria = this._check_expressions.Skip(1).Aggregate(this._check_expressions.First(), (acc, next) => acc + "\n&&\n" + next);
            
            var batch_processing = FormatComponent("BatchProcessing", new { IfCriteria = if_criteria.Indent(2) });
            
            var transform_function = (string)context.transform().Accept(this);

            string exportFTPProcessContextComponent = GetComponent("ExportFTPProcessContext");
            
            var main_component = new ExportFTPProcessContextComponent
            {
                EventListeners = this._event_commands.SelectMany(_ => _.Value).Aggregate("", (acc, next) => acc + "\n" + next.ctor_sub.Indent(2)),
                InitializeContextMethod = BuildInitializeContextMethod().Indent().Indent(),
                TransformFunction = transform_function.Indent().Indent(),
                ImageProcessing = image_processing.Indent().Indent(),
                BatchProcessing = batch_processing.Indent().Indent(),
                AttemptSftpUpload = Buildattempt_sftp_upload().Indent().Indent(),
                PrivateHelperMethods = this._private_helper_methods.Aggregate("", (acc, next) => acc + next).Indent().Indent(),
                SqlHelper = FormatComponent("SQL", new { }),
                PreCheckFunction = FormatComponent("PreCheck", new { Options = this._sftp_pre_check_options }),
                DeleteOriginalFilesOption_many = WriteHandleDeleteOriginalFilesOption_many_Function().Indent().Indent(),
                DeleteOriginalFilesOption = WriteHandleDeleteOriginalFilesOption_Function().Indent().Indent(),
                IncludeToHashSetPublicStaticExtension = IncludeToHashSetPublicStaticExtension().Indent(),
                RecursiveDirectoryNuke = GetComponent("RecursiveDirectoryNuke")
            };

            return Smart.Format(exportFTPProcessContextComponent, main_component);
        }

        public override object VisitEvents([NotNull] ExportFTPParser.EventsContext context)
        {
            foreach (var ctx in context.event_expression())
            {
                ctx.Accept(this);
            }
            return null; //.Aggregate("", (acc, next) => acc + "\n" + next).Indent();
        }

        DualKeyDictionary<List<(string kind, string ctor_sub, string update)>> _event_commands = new DualKeyDictionary<List<(string kind, string ctor_sub, string update)>>();
        private List<string> _cleanup_channel;
        private int _cleanup_channel_iterator;

        public override object VisitEvent_expression([NotNull] ExportFTPParser.Event_expressionContext context)
        {
            var type = context.Try(ctx => ctx.actionType.Text, null);
            var name = context.Try(ctx => ctx.actionName.STRING_ANY().GetText(), null);

            int channelIterator = 0;

            string GetChannelName()
            {
                return $"type_{(type == null ? "" : type)}_name_{(name == null ? "" : name)}_{channelIterator++}";
            }
            
            string channelName = $"type_{(type == null ? "" : type)}_name_{(name == null ? "" : name)}_{this.email_channel_iterator++}";
            var id = GetActionIdentifier(context);

            if (this._event_commands.ContainsKey(id) == false)
                this._event_commands[id] = new List<(string kind, string ctor_sub, string update)>();
            // now it's time to decompile what our commands are!
            this._event_commands[id].AddRange(context.event_routing().Select(ctx =>  SelectEvent(ctx, context.scenario.Text, GetChannelName())));
            return null;
        }

        private (string kind, string ctorsub, string update) SelectEvent(ExportFTPParser.Event_routingContext context, string scenario, string channelName)
        {
            switch (context.event_handler_name.Text)
            {
                case "email":
                    return SelectEmailEvent(context, scenario, channelName);
                case "do":
                    return SelectDoEvent(context, scenario, channelName);
                //default:
                //    return BuildMoveAction();
                default: throw new Exception("Something bad happened when trying to route events");
            }
        }

        private (string kind, string ctorsubStub, string updateStub) SelectDoEvent(ExportFTPParser.Event_routingContext context, string scenario, string channelName)
        {
            this._type = typeof(void);
            var action = context.event_routing_option().First().general_expression().Accept(this) as string;

            return
            (
                scenario,
                $"this._reporter.Subscribe(ActionMessageListener.GetActionMessageListener(this._efk, {action}), true, \"{channelName}\");",
                $"this._reporter.Update(\"{channelName}\", \"ActionMessageListeners do not utilize this message.\");"
            );
        }

        private (string kind, string ctorsub, string update) SelectEmailEvent(ExportFTPParser.Event_routingContext context, string scenario, string channelname)
        {
            // this._reporter.Subscribe("EMAIL_SFTP_FAIL_2", new EmailWriter(IEnumerable<string> to, string subject))
            var toList = context.event_routing_option().First(_ => _.transform_action_option().name.Text == "to").transform_action_option().@string().STRING_ANY().GetText();
            var subject = context.event_routing_option().First(_ => _.transform_action_option().name.Text == "subject").transform_action_option().@string().STRING_ANY().GetText();
            var body = GetTransformActionAsStringOrGeneralExpression(context.event_routing_option().First(_ => _.transform_action_option().name.Text == "body").transform_action_option(), true);
            
            return (
                scenario,
                $"this._reporter.Subscribe(new EmailWriter(\"{toList}\".Split('|'), \"{subject}\"), true, \"{channelname}\");",
                $"this._reporter.Update(\"{channelname}\", {body});"
            );
        }

        private static string GetActionIdentifier(ExportFTPParser.Event_expressionContext context) =>
            $"{context.Try(c => c.actionType.Text, "")}" +
            $":" +
            $"{context.Try(c => c.actionName.STRING_ANY().GetText(), "")}";

        private string BuildInitializeContextMethod()
        {
            var keys = this._initialize_context_transform_reset_commands.Select(key => new InitializeContextMethodComponent { Key = key });
            using (var stream = Assembly
                .GetExecutingAssembly()
                .GetManifestResourceStream("DocuStream.Grammar.ExportFTP.ScriptCompilation.FormattingComponents.InitializeContextMethod.component.txt"))
            {
                return Smart.Format(new StreamReader(stream).ReadToEnd(), new object[] { keys });
            }
        }

        private string Buildattempt_sftp_upload()
        {
            // this one is static, no need to do any smart replacements.
            using (var stream =
                Assembly
                .GetExecutingAssembly()
                .GetManifestResourceStream("DocuStream.Grammar.ExportFTP.ScriptCompilation.FormattingComponents.AttemptSftpUpload.component.txt"))
            {
                return Smart.Format(new StreamReader(stream).ReadToEnd());
            }
        }

        public override object VisitString([NotNull] ExportFTPParser.StringContext context)
        {
            return context.STRING_ANY().GetText();
        }

        public override object VisitGeneral_expression([NotNull] ExportFTPParser.General_expressionContext context)
        {
            string body;
            if (context.CSHARP_ANY().GetText().StartsWith("{", StringComparison.CurrentCulture))
            {
                
                body = context.CSHARP_ANY().GetText().Trim('{').Trim('}');
            }
            else
            {
                body = $"return {context.CSHARP_ANY().GetText().Trim('\n', '\t', '\r')};";
            }
            if (context.label_it != null)
            {
                if (this._type == typeof(void))
                    throw new TypeAccessException("the type " + typeof(void) + " cannot be used as a return type for general expressions.");

                if (body.Contains("return") == false)
                    throw new ArgumentException("The block-expression " + context.CSHARP_ANY().GetText() + " was expected to contain a return statement at the end of it.");

                if (this._type == typeof(string))
                {
                    var label = context.label_it.STRING_ANY().GetText();
                    body =
                        body.Substring(0, body.LastIndexOf("return", StringComparison.Ordinal))
                        + "strings[\"" + label + "\"] =" +
                                body.Substring(body.LastIndexOf("return", StringComparison.Ordinal) + 6, (body.Length - (body.LastIndexOf("return", StringComparison.Ordinal) + 6)));
                    body += "\n";
                    body += "return strings[\"" + label + "\"];";
                }
                else
                {
                    var label = context.label_it.STRING_ANY().GetText();
                    body =
                        body.Substring(0, body.LastIndexOf("return", StringComparison.Ordinal))
                        + "objects[\"" + label + "\"] =" +
                                body.Substring(body.LastIndexOf("return", StringComparison.Ordinal) + 6, (body.Length - (body.LastIndexOf("return", StringComparison.Ordinal) + 6)));
                    body += "\n";
                    body += "return (" + this._type.GetFriendlyName() + ")(objects[\"" + label + "\"]);";
                }
            }

            var fn_name = $"_c_{_general_expression_iterator++}";

            this._private_helper_methods.Add(FormatComponent("GeneralExpression", new { FunctionName = fn_name, Body = body.Indent(), Type = _type.GetFriendlyName() }));

            return fn_name;
        }

        public override object VisitWhere_expression([NotNull] ExportFTPParser.Where_expressionContext context)
        {
            this._type = typeof(bool);
            return FormatComponent("Where", new { FunctionName = context.general_expression().Accept(this) });
        }

        public override object VisitInput_let_identifier([NotNull] ExportFTPParser.Input_let_identifierContext context)
        {
            string key = "f477f3a2793349d5903305d7bfa9d96d";

            if (context != null)
            {
                key = (string)context.@string().Accept(this);
            }

            this._input_staging_key = key;

            return FormatComponent("LetExpression", new { Key = key });
        }

        public override object VisitDir_expression([NotNull] ExportFTPParser.Dir_expressionContext context)
        {
            // return string if path_str, else return hook for path_gen, and go ahead and memoize that check body.
            string dir = null;
            string where = "";
            if (context.path_gen != null)
            {
                this._type = typeof(string);
                dir = (string)context.path_gen.Accept(this);
            }
            else if (context.path_str != null)
            {
                dir = context.path_str.GetText();
            }

            if (context.where_expression() != null)
            {
                where = "." + context.where_expression().Accept(this);
            }

            string let_code = (string)this.VisitInput_let_identifier(context.input_let_identifier());
            if (context.check_condition() != null)
            {
                context.check_condition().Accept(this);
            }

            string files_pickup =
                    $"var dir_name = {dir}(this._efk);\n" +
                    $"var files = new HashSet<EFFile> {{}};\n" +
                    $"if (Directory.Exists(dir_name)) \n" +
                    $"{{\n" +
                    (
                            $"files = Directory.GetFiles(dir_name)\n" +
                                                $".Select(f => new EFFile {{ Image = i,File = new FileInfo(f) }})".Indent(6) + "\n" +
                                                $"{where}.ToHashSet();".Indent(6)
                            + "\n"
                    ).Indent() + "\n"
                    + "}\n"
                    + let_code;

            return files_pickup;

        }

        // returns string
        public override object VisitInput([NotNull] ExportFTPParser.InputContext context)
        {
            if (context.input_and_finally_expression() != null)
                context.input_and_finally_expression().and_finally_check_condition().ToList().ForEach(ctx =>
                ctx.Accept(this)
            );

            return context.input_expression().Aggregate(
                seed: "",
                func: (acc, next) => acc + $"\n{next.Accept(this)}\n"
            );
        }

        public override object VisitVar_where([NotNull] ExportFTPParser.Var_whereContext context)
        {
            string where = "";
            if (context.where_expression() != null)
            {
                where = "." + context.where_expression().Accept(this);
            }
            return
                $"this._input_staging[\"{context.var_name.STRING_ANY().GetText()}\"]\n" +
                                $"{where}".Indent(4);
        }

        public override object VisitAnd_finally_check_condition([NotNull] ExportFTPParser.And_finally_check_conditionContext context)
        {
            // do var-where's
            IEnumerable<string> var_wheres = context.var_where().Select(ctx => (string)ctx.Accept(this)).ToList();
            string input = var_wheres.Skip(1).Aggregate(
                seed: var_wheres.First(),
                func: (acc, next) => acc + $"\n.Union({next})".Indent()) + ".ToHashSet()";

            return CreateCheckCondition(context, input);
        }

        public override object VisitCheck_condition([NotNull] ExportFTPParser.Check_conditionContext context)
        {

            return CreateCheckCondition(context, $"this._input_staging[\"{this._input_staging_key}\"]");
        }

        private string CreateCheckCondition(ExportFTPParser.Check_conditionContext context, string input)
        {
            string presence = "";
            string action = (context.action.Type == ExportFTP.ExportFTPLexer.FAILS ? "false" : "true");
            string special_action = "";
            if (context.check_condition_completion().filter != null)
            {
                string sub_general_expression = null;
                this._type = typeof(bool);
                sub_general_expression = (string)context.check_condition_completion().general_expression().Accept(this);
                string filter = context.check_condition_completion().filter.Type == ExportFTPLexer.ANY ? "Any" : "All";
                string condition = context.check_condition_completion().condition.Type == ExportFTPLexer.TRUE ? "true" : "false";
                special_action =
                    $"{action} == effs.{filter}(eff =>\n" +
                    $"{{\n" + (
                            $"efk.Image = eff.Image;\n" +
                            $"efk.FileInfo = eff.File;\n" +
                            $"efk.AllFiles = effs;\n" +
                            $"return {condition} == {sub_general_expression}(efk);"
                    ).Indent() + "\n" +
                    $"}})\n";
            }
            if (context.check_condition_completion().presence != null)
            {
                if (context.check_condition_completion().presence.Type == ExportFTPLexer.ANYPRESENT)
                {
                    presence = $"{action} == (effs.Count() > 0) {(special_action == "" ? "" : $"{(action == "true" ? "||" : "&&")}\n")}";
                }
                else
                {
                    presence = $"{action} == (effs.Count() < 1) {(special_action == "" ? "" : $"{(action == "true" ? "||" : "&&")}\n")}";
                }
            }
            string general_expression = $"_c_{_general_expression_iterator++}";
            this._private_helper_methods.Add(
                $"private Func<ISet<EFFile>, bool> {general_expression}(EFKit efk)\n" +
                $"{{\n" + (
                        $"return effs => {{\n" + (
                                $"var f = efk.FileInfo;\n" +
                                $"var d = efk.AllFiles;\n" +
                                $"var i = efk.Image;\n" +
                                $"var t = efk.DateTimeTool;\n" +
                                $"var c = efk.CounterTool;\n" +
                                $"var ini = efk.State;\n" + $"var let = this._input_staging;\n" +
                                $"return \n" +
                                        $"{presence}{special_action};".Indent()
                        ).Indent() + $"\n}};"
                ).Indent() + $"\n}}\n"
            );

            _check_expressions.Add($"{general_expression}(this._efk)({input})");
            return $"{general_expression}(this._efk)({input})";
        }

        private string CreateCheckCondition(ExportFTPParser.And_finally_check_conditionContext context, string input)
        {
            string presence = "";
            string action = (context.action.Type == ExportFTP.ExportFTPLexer.FAILS ? "false" : "true");
            string special_action = "";
            if (context.check_condition_completion().filter != null)
            {
                string sub_general_expression = null;
                this._type = typeof(bool);
                sub_general_expression = (string)context.check_condition_completion().general_expression().Accept(this);
                string filter = context.check_condition_completion().filter.Type == ExportFTPLexer.ANY ? "Any" : "All";
                string condition = context.check_condition_completion().condition.Type == ExportFTPLexer.TRUE ? "true" : "false";
                special_action =
                    $"{action} == effs.{filter}(eff =>\n" +
                    $"{{\n" + (
                            $"efk.Image = eff.Image;\n" +
                            $"efk.FileInfo = eff.File;\n" +
                            $"efk.AllFiles = effs;\n" +
                            $"return {condition} == {sub_general_expression}(efk);"
                    ).Indent() + "\n" +
                    $"}})\n";
            }
            if (context.check_condition_completion().presence != null)
            {
                if (context.check_condition_completion().presence.Type == ExportFTPLexer.ANYPRESENT)
                {
                    presence = $"{action} == (effs.Count() > 0) {(special_action == "" ? "" : $"{(action == "true" ? "||" : "&&")}\n")}";
                }
                else
                {
                    presence = $"{action} == (effs.Count() < 1) {(special_action == "" ? "" : $"{(action == "true" ? "||" : "&&")}\n")}";
                }
            }

            // (S u T) ` =  S` n T`
            // (S n T) ` =  S` u T`

            // which fails when ()
            // which succeeds when (_f() || any-present)

            string general_expression = $"_c_{_general_expression_iterator++}";


            this._private_helper_methods.Add(
                FormatComponent("CheckCondition", new
                {
                    FunctionName = general_expression,
                    Presence = presence,
                    SpecialAction = special_action
                })
            );
            _check_expressions.Add($"{general_expression}(this._efk)({input})");
            return $"{general_expression}(this._efk)({input})";
        }

        public override object VisitInput_expression([NotNull] ExportFTPParser.Input_expressionContext context)
        {
            if (context.path_expression() != null)
                throw new NotSupportedException("Not doing paths yet.");

            int a = context.start.StartIndex;
            int b = context.stop.StartIndex;
            var function_name = "_imageProcess_" + context.dir_expression().input_let_identifier().@string().STRING_ANY().GetText().Replace("-", "_");
            this._private_helper_methods.Add(
                FormatComponent("ImageProcessingAction",
                   new {
                       FunctionName = function_name,
                       Body = ((string)context.dir_expression().Accept(this)).Indent(),
                       Comment = TokenStream.GetText(context).Replace("\r\n", "\n").Replace("\n", "\n//".Indent())
                   }
               )
            );

            return function_name + "(i);";
        }

        public override object VisitSimple_transform_action_chain([NotNull] ExportFTPParser.Simple_transform_action_chainContext context)
        {
            string files = $"files_{ _transform_file_snapshot_counter++}";
            string assignment = $"var {files} = {context.transform_action().Accept(this)}({_action_files_out_stack.Peek()});\n";
            _action_files_out_stack.Push(files);
            return assignment;
        }

        public override object VisitUnion_transform_action_chain([NotNull] ExportFTPParser.Union_transform_action_chainContext context)
        {
            // unions in -> (a,b,c,...) intend to pass 'in' to a, b, c, etc.
            // therefore, when a union is entered, the height of the stack is known, and this union visitor after coming up from a chain_context.Accept
            // will note the resulting top of the stack (denoting something that aught to be included in the result union)
            // and it will subsequently drain the stack down to the targetStackCount, and pass that result back into the next object.

            var targetStackCount = this._action_files_out_stack.Count();

            var unionables = new List<string>() { };
            string assignment_list = "";
            foreach (var chain_context in context.transform_action_chain())
            {
                assignment_list += chain_context.Accept(this);
                unionables.Add(this._action_files_out_stack.Peek());
                // the result of this is that 'in' was just passed to a, now on the next iteration, 'in' also will go to b.
                while (this._action_files_out_stack.Count() != targetStackCount) { this._action_files_out_stack.Pop(); }
            }

            //now, upon leaving a union stack, we must push a united entity to the caller.
            var pushed = unionables.Skip(1).Aggregate(unionables.First(), (acc, next) => acc + $".Union({next})") + (unionables.Count() > 1 ? ".ToHashSet()" : "");
            this._action_files_out_stack.Push(pushed);
            return assignment_list;
        }

        bool _isCleanUpInUse = false;
        public override object VisitCleanup_transform_action_chain([NotNull] ExportFTPParser.Cleanup_transform_action_chainContext context)
        {
            if (_isCleanUpInUse)
            {
                throw new Exception("Nested [cleanup] labels detected, this means nothing to the XgAutoCompiler!");
            }
            _isCleanUpInUse = true;
            
            string cleanup_name = $"cleanup_{_general_expression_iterator++}";
            string cleanup_channel = $"\"CLX{_cleanup_channel_iterator++}\"";
            this._cleanup_channel.Add(cleanup_channel);
            string tgtFiles = _action_files_out_stack.Peek();
            var files = "new HashSet<EFFile>(" + tgtFiles + ")";
            // since we're binding, we need to pass in that correct input name.
            var body = ((string)context.tail.Accept(this)).Replace(tgtFiles, "_files");

            string cleanupCbk = $"(ISet<EFFile> _files) => {{\n{body.Indent()}\n}}";
            _isCleanUpInUse = false;
            return FormatComponent("CleanUp", new {
                        Files    = files,
                        Handle   = cleanup_name,
                        Callback = cleanupCbk.Indent(3),
                        Channel  = cleanup_channel
                    });
        }

        // left ->> right
        public override object VisitGrouping_transform_action_chain([NotNull] ExportFTPParser.Grouping_transform_action_chainContext context)
        {
            string assignment_list =
                (string)context.head.Accept(this)
                +
                (string)context.tail.Accept(this);

            return assignment_list;
        }

        public override object VisitTransform_where_expression([NotNull] ExportFTPParser.Transform_where_expressionContext context)
        {
            this._type = typeof(bool);
            WhereComponent component = new WhereComponent
            {
                FunctionName = (string)context.general_expression().Accept(this)
            };

            return FormatComponent("Where", component);
        }

        public static string FormatComponent<T>(string formattingComponentName, T formattingObject)
        {
            using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream($"DocuStream.Grammar.ExportFTP.ScriptCompilation.FormattingComponents.{formattingComponentName}.component.txt"))
            {
                using (var reader = new StreamReader(stream))
                {
                    return Smart.Format(reader.ReadToEnd(), formattingObject);
                }
            }
        }

        public string GetComponent(string formattingComponentName)
        {
            using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream($"DocuStream.Grammar.ExportFTP.ScriptCompilation.FormattingComponents.{formattingComponentName}.component.txt"))
            {
                using (var reader = new StreamReader(stream))
                {
                    return reader.ReadToEnd();
                }
            }
        }

        // TODO CLEANUP SUPPORT
        public override object VisitTransform_action_expression([NotNull] ExportFTPParser.Transform_action_expressionContext context)
        {
            // reset file push stack. this is a necessary operation to account for the fact that
            // root level []->[] expressions will leak their file feeds.
            this._action_files_out_stack.Clear();
            this._action_files_out_stack.Push("files");

            var key = (string)context.transform_pick_up_expression().Accept(this);
            var where_expression = context.transform_where_expression() != null ? $".{context.transform_where_expression().Accept(this)}" : "";
            var transform_action_expression_method_name = $"_transform{ this._transform_action_counter}";

            this._transform_action_sub_counter = 0;
            this._transform_file_snapshot_counter = 0;
            var method_body = context.transform_action_chain().Accept(this);
            this._private_helper_methods.Add(
                $"private void {transform_action_expression_method_name}(ISet<EFFile> files)\n" +
                $"{{\n" +
                        $"{method_body}".Indent() + "\n" +
                $"}}\n"
            );

            this._transform_action_counter++;
            List<string> cleanup_channel = new List<string>(this._cleanup_channel);
            this._cleanup_channel.Clear();
            // this return port not actually utilized
            return new TransformComponent
            {
                TransformKey = key,
                TransformFunctionName = transform_action_expression_method_name,
                WhereClause = where_expression,
                CleanUpChannel = cleanup_channel
            };
        }

        public override object VisitTransform([NotNull] ExportFTPParser.TransformContext context)
        {
            // populates _transformComponents
            var transforms = context.transform_action_expression().Select(ctx => 
            (TransformComponent)ctx.Accept(this)).ToList();



            return FormatComponent("Transform", transforms);
        }

        public override object VisitDir_general_expression_transform([NotNull] ExportFTPParser.Dir_general_expression_transformContext context)
        {
            throw new NotSupportedException("Haven't done it yet.");
        }
        public override object VisitDir_string_transform([NotNull] ExportFTPParser.Dir_string_transformContext context)
        {
            throw new NotSupportedException("Haven't done it yet.");
        }
        public override object VisitPath_general_expression_transform([NotNull] ExportFTPParser.Path_general_expression_transformContext context)
        {
            throw new NotSupportedException("Haven't done it yet.");
        }
        public override object VisitPath_string_transform([NotNull] ExportFTPParser.Path_string_transformContext context)
        {
            throw new NotSupportedException("Haven't done it yet.");
        }
        public override object VisitVar_transform([NotNull] ExportFTPParser.Var_transformContext context)
        {
            string var_name = context.@string().STRING_ANY().GetText();
            this._initialize_context_transform_reset_commands.Add(var_name);
            return var_name;
        }

        public override object VisitTransform_action([NotNull] ExportFTPParser.Transform_actionContext context)
        {
            string transform_action_body = GetTransformAction(context.name.Text, context.transform_action_option());
            string text  = context.Try(ctx => ctx.name.Text, null);
            string label = context.Try(ctx => ctx.label.STRING_ANY().GetText(), null);
            IEnumerable<List<(string kind, string ctor_sub, string update)>> enumerable = this._event_commands.LookupMatchesTo(text, label);
            if (enumerable.Any())
            {
                transform_action_body =
                    FormatComponent(
                        "Event", 
                        new
                        {
                            Body = transform_action_body,
                            SuccessEvents = enumerable
                                                .SelectMany(_ => _)
                                                .Where(_ => _.kind == "succeeds")
                                                .Select(_ => _.update)
                                                .Aggregate("", (acc, next) => acc + "\n" + (next as string).Indent()),
                            FailureEvents = enumerable
                                                .SelectMany(_ => _)
                                                .Where(_ => _.kind == "fails")
                                                .Select(_ => _.update)
                                                .Aggregate("", (acc, next) => acc + "\n" + (next as string).Indent())
                        }
                    );
            }
            string transform_action_name = $"_c_transform_action{_transform_action_counter}_{_transform_action_sub_counter++}_{context.name.Text}";
            this._private_helper_methods.Add(
                $"private ISet<EFFile> {transform_action_name}(ISet<EFFile> files)\n" +
                $"{{\n" +
                        $"{transform_action_body}".Indent() + "\n" +
                $"}}\n"
            );
            return transform_action_name;
        }

        public string GetTransformAction(string action, ExportFTPParser.Transform_action_optionContext[] options)
        {
            switch (action)
            {
                case "aggregate": return BuildAggregateAction(options);
                case "zip": return BuildZipAction(options);
                case "move": return BuildMoveAction(options);
                case "copy": return BuildCopyAction(options);
                case "id": return BuildIdAction();
                case "nil": return BuildNilAction();
                case "tag": return BuildTagAction(options);
                case "pgp": return BuildPgpAction(options);
                case "gpg": return BuildGpgAction(options);
                case "sftp": return BuildSftpAction(options);
                case "ftp": return BuildFtpAction(options);
                default: return "return new HashSet<EFFile> { };";
            }
        }

        private int GetMilliSecondsTimeOut(ExportFTPParser.Transform_action_optionContext[] options)
        {
            var timeout_seconds = 60;
            ExportFTPParser.Transform_action_optionContext timeout;

            if ((timeout = options.FirstOrDefault(_ => _.name.Text == "timeout-seconds")) != null)
            {
                timeout_seconds = int.Parse(timeout.@string().STRING_ANY().GetText());
            }

            return timeout_seconds * 1000;
        }
        
        private List<object> _sftp_pre_check_options;

        private string BuildSftpAction(ExportFTPParser.Transform_action_optionContext[] options)
        {
            var destination = GetTransformActionAsStringOrGeneralExpression(options.First(_ => _.name.Text == "directory"), true);

            var set_as_default_sftp_options = options.Any(_ => _.name.Text == "specify-default-sftp-options");
            string user                     = null;
            string password                 = null;
            string server                   = null;
            string host_finger              = null;
            string ppk_passphrase           = "";
            string ppk_path                 = "";
            int MAX_UPLOAD_ATTEMPTS         = 1;
            var timeout_seconds             = GetMilliSecondsTimeOut(options); //don't need the milliseconds distinction actually.
            var sftp_setup_string           = "";
            var sftpSetupStringFailureMessage = "";
            // user/pass/server can be optionally set if set_as_default_ftp_options is specified.

            if (options.Any(_ => _.name.Text == "user"))
                user = GetTransformActionAsStringOrGeneralExpression(options.First(_ => _.name.Text == "user"), true);

            if (options.Any(_ => _.name.Text == "password"))
                password = GetTransformActionAsStringOrGeneralExpression(options.First(_ => _.name.Text == "password"), true);

            if (options.Any(_ => _.name.Text == "server"))
                server = GetTransformActionAsStringOrGeneralExpression(options.First(_ => _.name.Text == "server"), true);

            if (options.Any(_ => _.name.Text == "upload-attempts"))
                MAX_UPLOAD_ATTEMPTS = int.Parse(options.First(_ => _.name.Text == "upload-attempts").@string().STRING_ANY().GetText());
            
            if (options.Any(_ => _.name.Text == "use-host-key"))
                host_finger = GetTransformActionAsStringOrGeneralExpression(options.First(_ => _.name.Text == "use-host-key"), true);
            else
                host_finger =   $"this._session.ScanFingerprint( \n" +
                                $"                              new WinSCP.SessionOptions \n" +
                                $"                              {{\n" +
                                $"                                  Protocol = WinSCP.Protocol.Sftp,\n" +
                                $"                                  HostName = {server},\n" +
                                $"                              }},\n" +
                                $"                              \"SHA-256\"\n" +
                                $"                          )\n";

            if (options.Any(_ => _.name.Text == "ppk-path"))
                ppk_path = $"SshPrivateKeyPath = " +
                    $"{GetTransformActionAsStringOrGeneralExpression(options.First(_ => _.name.Text == "ppk-path"), true)},\n";
            
            if (options.Any(_ => _.name.Text == "ppk-passphrase"))
                ppk_passphrase = $"SshPrivateKeyPassphrase = " +
                    $"{GetTransformActionAsStringOrGeneralExpression(options.First(_ => _.name.Text == "ppk-passphrase"), true)},\n";




            if (set_as_default_sftp_options)
            {
                sftp_setup_string =
                    $"new SessionOptions" +
                    $"{{\n" +
                    $"  Protocol = WinSCP.Protocol.Sftp,\n" +
                    $"  HostName = (this._sftp_server = {server}), \n" +
                    $"  UserName = (this._sftp_user = {user}), \n" +
                    $"  Password = (this._sftp_password = {password}), \n" +
                    $"  TimeoutInMilliseconds = {timeout_seconds}, \n" +
                    $"  SshHostKeyFingerprint = {host_finger},\n" +
                    ppk_passphrase +
                    ppk_path +
                    $"}}";
                sftpSetupStringFailureMessage =
                    $"\"Protocol = SFTP,\\n" +
                    $"Host = \" + {server} + \"\\n" +
                    $"UserName = \" + {user} + \"\\n" +
                    $"Password = \" + {password} + \"\\n" +
                    $"Fingerprint = \" + {host_finger} + \"\\n" +
                    $"Ppk_Passphrase = \" + {(string.IsNullOrEmpty(ppk_passphrase) ? "\"null\"" : ppk_path)} + \"\\n" +
                    $"Ppk_Path = \" + {(string.IsNullOrEmpty(ppk_path) ? "\"null\"" : ppk_path)}";
            }
            
            else if (user != null && password != null && server != null)
            {
                sftp_setup_string =
                    $"new SessionOptions" +
                    $"{{\n" +
                    $"  Protocol = WinSCP.Protocol.Sftp,\n" +
                    $"  HostName = {server}, \n" +
                    $"  UserName = {user}, \n" +
                    $"  Password = {password}, \n" +
                    $"  TimeoutInMilliseconds = {timeout_seconds},\n" +
                    $"  SshHostKeyFingerprint = {host_finger},\n" +
                    ppk_passphrase +
                    ppk_path +
                    $"}}";
                sftpSetupStringFailureMessage =
                    $"\"Protocol = SFTP,\\n" +
                    $"Host = \" + {server} + \"\\n" +
                    $"UserName = \" + {user} + \"\\n" +
                    $"Password = \" + {password} + \"\\n" +
                    $"Fingerprint = \" + {host_finger} + \"\\n" +
                    $"Ppk_Passphrase = \" + {(string.IsNullOrEmpty(ppk_passphrase) ? "\"null\"" : ppk_path)} + \"\\n" +
                    $"Ppk_Path = \" + {(string.IsNullOrEmpty(ppk_path) ? "\"null\"" : ppk_path)}";
            }
            else
            {
                sftp_setup_string =
                    $"new SessionOptions" +
                    $"{{\n" +
                    $"  Protocol = WinSCP.Protocol.Sftp,\n" +
                    $"  HostName = this._sftp_server, \n" +
                    $"  UserName = this._sftp_user, \n" +
                    $"  Password = this._sftp_password, \n" +
                    $"  TimeoutInMilliseconds = {timeout_seconds},\n" +
                    $"  SshHostKeyFingerprint = {host_finger},\n" +
                    ppk_passphrase +
                    ppk_path +
                    $"}}";
                sftpSetupStringFailureMessage =
                    $"\"Protocol = SFTP,\\n" +
                    $"Host = \" + {server} + \"\\n" +
                    $"UserName = \" + {user} + \"\\n" +
                    $"Password = \" + {password} + \"\\n" +
                    $"Fingerprint = \" + {host_finger} + \"\\n" +
                    $"Ppk_Passphrase = \" + {(string.IsNullOrEmpty(ppk_passphrase) ? "\"null\"" : ppk_path)} + \"\\n" +
                    $"Ppk_Path = \" + {(string.IsNullOrEmpty(ppk_path) ? "\"null\"" : ppk_path)}";
            }

            this._sftp_pre_check_options.Add(new { ConnectionInfo = sftp_setup_string, ConnectionInfoStringified = sftpSetupStringFailureMessage});
            
            var sftpcomponent = new SftpFunctionComponent
            {
                SftpClientSetup = sftp_setup_string,
                Destination = destination
            };

            return FormatComponent("SftpFunction", sftpcomponent);
        }
        /* 
            [tag
                as: string
                where?: EFKitTo
            ] : [File] -> [File], 

            with the side effect that the next transform expression will 
            receive a NEFFS that contains the set of files named in our `as` 
            option with the same files we input here, optionally filtered by our where property.
         */
        string BuildTagAction(ExportFTPParser.Transform_action_optionContext[] options)
        {
            // what we plant to tag the files as.
            var tag = options.First(_ => _.name.Text == "as").@string().Accept(this);
            // where clause optional.
            var whereclause = "";
            var where = options.FirstOrDefault(_ => _.name.Text == "where");
            if (where != null)
                whereclause = FormatComponent("Where", new { FunctionName = where.general_expression().Accept(this) });
            var take = $"files{whereclause}";

            return FormatComponent("Tag", new { Take = take, Tag = tag });
        }

        string BuildFtpAction(ExportFTPParser.Transform_action_optionContext[] options)
        {
            
            var directory = GetTransformActionAsStringOrGeneralExpression(options.First(_ => _.name.Text == "directory"));
            var set_as_default_ftp_options = options.Any(_ => _.name.Text == "specify-default-ftp-options");
            string user = null;
            string password = null;
            string server = null;
            int MAX_UPLOAD_ATTEMPTS = 1;
            var timeout_milliseconds = GetMilliSecondsTimeOut(options) / 1000; // don't need the milliseconds distinction actually.
            
            // TODO: this is likely to cause issues. it is quite clear why. I'm just trying to get something built. --icr
            if (options.Any(_ => _.name.Text == "user"))
                user = (string)options.First(_ => _.name.Text == "user").@string().GetText();

            if (options.Any(_ => _.name.Text == "password"))
                password = (string)options.First(_ => _.name.Text == "password").@string().GetText();

            if (options.Any(_ => _.name.Text == "server"))
                server = (string)options.First(_ => _.name.Text == "server").@string().GetText();

            if (options.Any(_ => _.name.Text == "upload-attempts"))
                MAX_UPLOAD_ATTEMPTS = int.Parse((string)options.First(_ => _.name.Text == "server").@string().STRING_ANY().GetText());

            return "";
        }
        //  /*
        //      [
        //          copy
        //          destination: EFKit -> string
        //          use-both|use-originals|use-copies -- options
        //      ]
        //   */
        string BuildCopyAction(ExportFTPParser.Transform_action_optionContext[] options)
        {
            var destination = GetTransformActionAsStringOrGeneralExpression(options.First(_ => _.name.Text == "destination"));
            var include_source = false;
            var include_destination = false;
            var delete_originals = false;

            if (options.Any(_ => _.name.Text == "delete-original-files"))
                delete_originals = true;

            if (options.Any(_ => _.name.Text == "use-both"))
                include_destination = include_source = true;
            else if (options.Any(_ => _.name.Text == "use-originals"))
                include_source = true;
            else
                include_destination = true;
            var component = new { PathName = destination, DeleteOriginalFiles = delete_originals, IncludeSource = include_source, IncludeDestination = include_destination };
            return FormatComponent("Copy", component);
        }
        //  /*
        //      [id] : [f0: File] -> [f1: File] | f0 = f1
        //   */
        string BuildIdAction()
        {
            return $"return files;";
        }
        //  /*
        //      [nil] : [File] -> []
        //   */
        string BuildNilAction()
        {
            return "return new HashSet<EFFile> { };";
        }

        /*
              [
                pgp /or/ gpg
                delete-original-files
                exe: string
                to-user: string
                my-user: string
                passphrase: string
                sec-ring: string
                pub-ring: string
                timeout-seconds: string containing integer value
              ] : [File] -> [File]
         */
        private string BuildPgpAction(ExportFTPParser.Transform_action_optionContext[] options)
        {
            var exe        = GetTransformActionAsStringOrGeneralExpression(options.First(_ => _.name.Text == "exe"));
            var to_user    = GetTransformActionAsStringOrGeneralExpression(options.First(_ => _.name.Text == "to-user"));
            var my_user    = GetTransformActionAsStringOrGeneralExpression(options.First(_ => _.name.Text == "my-user"));
            var passphrase = GetTransformActionAsStringOrGeneralExpression(options.First(_ => _.name.Text == "passphrase"));
            var sec_ring   = GetTransformActionAsStringOrGeneralExpression(options.First(_ => _.name.Text == "sec-ring"));
            var pub_ring   = GetTransformActionAsStringOrGeneralExpression(options.First(_ => _.name.Text == "pub-ring"));  

            var timeout_milliseconds = GetMilliSecondsTimeOut(options);

            //var pgp = new PGP_DS(exe, passphrase, to_user, my_user, sec_ring, pub_ring, timeout_milliseconds);
            var delete_original_files = options.Any(_ => _.name.Text == "delete-original-files");

            return FormatComponent("Pgp", new { Exe = exe, Passphrase = passphrase, ToUser = to_user, MyUser = my_user, SecRing = sec_ring, PubRing = pub_ring, TimeoutMilliseconds = timeout_milliseconds , DeleteoriginalFiles = delete_original_files });
        }

        string BuildGpgAction(ExportFTPParser.Transform_action_optionContext[] options)
        {
            var exe = (string)options.First(_ => _.name.Text == "exe").@string().GetText();
            var to_user = (string)options.First(_ => _.name.Text == "to-user").@string().GetText();
            var my_user = (string)options.First(_ => _.name.Text == "my-user").@string().GetText();
            var passphrase = (string)options.First(_ => _.name.Text == "passphrase").@string().GetText();
            var sec_ring = (string)options.First(_ => _.name.Text == "sec-ring").@string().GetText();
            var pub_ring = (string)options.First(_ => _.name.Text == "pub-ring").@string().GetText();
            var timeout_milliseconds = GetMilliSecondsTimeOut(options);
            var delete_original_files = options.Any(_ => _.name.Text == "delete-original-files");
            return FormatComponent("Gpg", new { Exe = exe, Passphrase = passphrase, ToUser = to_user, MyUser = my_user, SecRing = sec_ring, PubRing = pub_ring, TimeoutMilliseconds = timeout_milliseconds, DeleteoriginalFiles = delete_original_files });
        }

        /*
            [
                aggregation
                name: 
                one-space-per-line?
                delete-original-files?
            ]: [File] -> [File]
         */
        string BuildAggregateAction(ExportFTPParser.Transform_action_optionContext[] options)
        {
            var name_option = options.First(o => o.name.Text == "name");
            var name_source = "";
            var one_space_per_line = options.Any(o => o.name.Text == "one-space-per-line");
            var accumulator_function = "";
            var delete_files = options.Any(o => o.name.Text == "delete-original-files");

            name_source = GetTransformActionAsStringOrGeneralExpression(name_option);

            if (one_space_per_line)
            {
                accumulator_function =
                    $"(acc, next) => \n" + (
                        $"acc.Trim(new char[] {{ '\n', '\r' }}) + (acc == \"\" ? \"\" : \"\n\") + " +
                        $"next.Replace(\"\r\n\", \"\n\").Replace(\"\n\n\", \"\n\")").Indent()
                    ;
            }
            else
            {
                accumulator_function = $"(acc, next) =>acc + next";
            }

            var component = new AggregateFunctionComponent
            {
                AccumulationFunction = accumulator_function,
                DeleteFiles = delete_files,
                NameSource = name_source
            };

            return FormatComponent("AggregateFunction", component);
        }

        private string GetTransformActionAsStringOrGeneralExpression(ExportFTPParser.Transform_action_optionContext name_option, bool useDoubleQuotes = false)
        {
            string name_source;
            if (name_option.@string() != null)
            {
                name_source = (string)name_option.@string().Accept(this);
                if (useDoubleQuotes)
                    name_source = "\"" + name_source + "\"";
            }
            else if (name_option.name.Text != "where")
            {
                this._type = typeof(string);
                name_source = name_option.general_expression().Accept(this) + "(this._efk)";
            }
            else
            {
                this._type = typeof(bool);
                WhereComponent component = new WhereComponent
                {
                    FunctionName = (string)name_option.general_expression().Accept(this)
                };

                return FormatComponent("Where", component);
            }
            return name_source;
        }

        /*
         
            [
                zip
                name: EFKit->String
                delete-original-files
                compression-level: string (must contain one of the values specified in TransformActionVisitor.cs.appendix a, defaults to "Default")
            ]: [File] -> [File] (!!! prerequisite that all zips be in same directory)
         */
        /**TransformActionVisitor.cs.appendix a
         * compression-level options: None = 0, Level0 = 0, BestSpeed = 1, Level1 = 1, Level2 = 2, Level3 = 3, Level4 = 4, Level5 = 5, Default = 6, Level6 = 6, Level7 = 7, Level8 = 8, BestCompression = 9,  Level9 = 9
         */

        string BuildZipAction(ExportFTPParser.Transform_action_optionContext[] options)
        {

            var name = GetTransformActionAsStringOrGeneralExpression(options.First(_ => _.name.Text == "name"));
                        
            var delete_original_files = options.Any(_ => _.name.Text == "delete-original-files");
            var p = options.FirstOrDefault(_ => _.name.Text == "compression-level");
            var compression_level_assignment = $"CompressionLevel.{(p == null ? "Default" : p.@string().STRING_ANY().GetText())};";
            string root = "\"\"";

            if (options.Any(_ => _.name.Text == "root"))
            {
                root = GetTransformActionAsStringOrGeneralExpression(options.First(_ => _.name.Text == "root"), true);
            }
            // I am Groot. -- Groot
            if (options.Any(_ => _.name.Text == "groot"))
            {
                throw new Exception("We are Groot.");
            }

            return FormatComponent("Zip", new { Name = name, DeleteOriginalFiles = delete_original_files, CompressionLevelAssignment = compression_level_assignment, Root = root });
        }

        /*
            [
                move
                destination: EFKit->File->String
            ]: [File] -> [File]
         */
        string BuildMoveAction(ExportFTPParser.Transform_action_optionContext[] options)
        {
            var destination = GetTransformActionAsStringOrGeneralExpression(options.First(_ => _.name.Text == "destination"));
            var attemptDeleteDir = options.Any(_ => _.name.Text == "remove-empty-parents");
            return FormatComponent("Move", new { Name = destination, DeleteParentsRecursive = attemptDeleteDir });
        }
        
        static string WriteHandleDeleteOriginalFilesOption_Function()
        {
            return 
                $"private void DeleteFile(EFFile file) \n" +
                $"{{\n" + (
                        $"File.Delete(file.File.FullName);\n" +
                        $"this._reporter.Update(\"LOG\", \"Deleted file '\" + file.File.FullName + \"'\");" + 
                        $"if (Directory.GetFiles(file.File.Directory.FullName).Count() == 0)/* future TODO: && dont_delete_dir == true */\n" +
                        $"{{\n" +
                                $"Directory.Delete(file.File.Directory.FullName);".Indent() + "\n" +
                        $"}}"
                ).Indent() + "\n" +
                $"}}\n";
        }

        static string CallDeleteFile(string file_parameter_name)
        {
            return $"DeleteFile({file_parameter_name})";
        }

        static string CallDeleteOriginalFiles(string files_parameter_name)
        {
            return $"DeleteOriginalFiles({files_parameter_name})";
        }

        public static string IncludeToHashSetPublicStaticExtension()
        {
            return
                $"// credit to: http://www.extensionmethod.net/1962/csharp/ienumerable-t/tohashset-t\n" +
                $"public static class LinqExtension\n" +
                $"{{\n" +
                $"    /// <summary>\n" +
                $"    /// Converts an IEnumerable to a HashSet\n" +
                $"    /// </summary>\n" +
                $"    /// <typeparam name=\"T\">The IEnumerable type</typeparam>\n" +
                $"    /// <param name=\"enumerable\">The IEnumerable</param>\n" +
                $"    /// <returns>A new HashSet</returns>\n" +
                $"    public static HashSet<T> ToHashSet<T>(this IEnumerable<T> enumerable)\n" +
                $"    {{\n" +
                $"        HashSet<T> hs = new HashSet<T>();\n" +
                $"        foreach (T item in enumerable)\n" +
                $"            hs.Add(item);\n" +
                $"        return hs;\n" +
                $"    }}\n" +
                $"}}";
        }

        // I am Groot. -- Groot
        static string WriteHandleDeleteOriginalFilesOption_many_Function()
        {
            return
                $"private void DeleteOriginalFiles(ISet<EFFile> files) \n" +
                $"{{\n" + ( $"foreach (var file in files)\n" + $"{{\n" + ($"DeleteFile(file);\n" + $"}}").Indent() )+ "\n" + $"}}\n" ;
        }
    }

    public static class TypeNameExtensions
    {
        public static string Indent(this string source, int tabs_count = 1)
        {
            return new string('\t', tabs_count) + source.Replace("\r\n", "\n").Replace("\n", "\n" + new string('\t', tabs_count));
        }

        public static string GetFriendlyName(this Type type)
        {
            if (type == typeof(void))
                return "void";
            if (type == typeof(string))
                return "string";
            if (type == typeof(int))
                return "int";
            if (type == typeof(object))
                return "object";

            string friendlyName = type.Name;
            if (type.IsGenericType)
            {
                int iBacktick = friendlyName.IndexOf('`');
                if (iBacktick > 0)
                {
                    friendlyName = friendlyName.Remove(iBacktick);
                }
                friendlyName += "<";
                Type[] typeParameters = type.GetGenericArguments();
                for (int i = 0; i < typeParameters.Length; ++i)
                {
                    string typeParamName = typeParameters[i].IsGenericType ? GetFriendlyName(typeParameters[i]) : typeParameters[i].Name;
                    friendlyName += (i == 0 ? typeParamName : "," + typeParamName);
                }
                friendlyName += ">";
            }

            return friendlyName;
        }
    }

    // credit to: http://www.extensionmethod.net/1962/csharp/ienumerable-t/tohashset-t
    public static class LinqExtension
    {
        /// <summary>
        /// Converts an IEnumerable to a HashSet
        /// </summary>
        /// <typeparam name="T">The IEnumerable type</typeparam>
        /// <param name="enumerable">The IEnumerable</param>
        /// <returns>A new HashSet</returns>
        // I am Groot. -- Groot
        public static HashSet<T> ToHashSet<T>(this IEnumerable<T> enumerable)
        {
            HashSet<T> hs = new HashSet<T>();
            foreach (T item in enumerable)
                hs.Add(item);
            return hs;
        }
    }
}
