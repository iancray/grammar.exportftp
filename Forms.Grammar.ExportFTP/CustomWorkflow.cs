﻿using System;
using DocuStream.ProcessingFramework.Application;
using DocuStream.ProcessingFramework.Data;
using DocuStream.ProcessingFramework.Reporting;
using DocuStream.ProcessingFramework.Utilities;

namespace Forms.Grammar.ExportFTP
{
    public class CustomWorkflow : Workflow
    {
        Action _transformCall;
        public CustomWorkflow(Action transformCall, IEnvironmentState state, IBatchEntryProvider batchProvider, string moduleName, IReporter reporter) 
        : base(state, batchProvider, moduleName, reporter)
        {
            this._transformCall = transformCall;
        }

        public override void NoRunnableBatchFound()
        {
            this._transformCall();
            this.Reporter.Update("STATUS", "None");
        }
    }
}
