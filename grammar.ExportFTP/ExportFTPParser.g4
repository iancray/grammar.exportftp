﻿parser grammar ExportFTPParser;



options { tokenVocab=ExportFTPLexer; } 
/*
 * Parser Rules
 */

/* compilable bits */
compileUnit 
	:	input transform events?
	;

input
	:	INPUT OpenParen input_expression+ input_and_finally_expression? CloseParen 
	;

transform
	: TRANSFORM OpenParen transform_action_expression+ CloseParen
	;

/* general bits */
string
	: (STRING_MODE__START | TRANSFORM_ACTION_MODE__STRING_START) STRING_ANY STRING_MODE__END
	;

general_expression 
	: (CSHARP_MODE__START | TRANSFORM_ACTION_MODE__CSHARP_START) expr_content=CSHARP_ANY CSHARP_MODE__END ((AS | TRANSFORM_ACTION_MODE__AS) label_it=string)?
	;
	
	
/*===============*/
/*=====EVENTSS====*/
events
	:  EVENTS OpenParen event_expression* CloseParen
	;

event_expression
	:	PLUS OpenParen 
		actionType=JustText?
		actionName=string?
		scenario=(FAILS|SUCCEEDS)
		(event_routing Comma?)*
		CloseParen
	;

event_routing
	:	TRANSFORM_ACTION_MODE__START
		event_handler_name=TRANSFORM_ACTION_TEXT
		event_routing_option+
		TRANSFORM_ACTION_END
	;

event_routing_option 
	: general_expression
	| transform_action_option
	;

/*===============*/
/*===TRANSFORM===*/
transform_action_option
	: name=TRANSFORM_ACTION_TEXT												//#basic_transform_option
	| name=TRANSFORM_ACTION_TEXT TRANSFORM_ACTION_COLON string					//#string_transform_option
	| name=TRANSFORM_ACTION_TEXT TRANSFORM_ACTION_COLON general_expression		//#general_expression_transform_option
	;

transform_action
	: TRANSFORM_ACTION_MODE__START
		name=TRANSFORM_ACTION_TEXT
		label=string?
		transform_action_option*
	 TRANSFORM_ACTION_END
	;
	
transform_action_chain
	: '(' transform_action_chain ( Comma transform_action_chain)* ')'							# union_transform_action_chain
	| head=transform_action_chain  TRANSFORM_CONTINUATION_ARROW tail=transform_action_chain		# grouping_transform_action_chain
	| CleanUpAction TRANSFORM_CONTINUATION_ARROW tail=transform_action_chain					# cleanup_transform_action_chain
	| target_action=transform_action															# simple_transform_action_chain
	;
	
transform_pick_up_expression
	: DIR general_expression		#dir_general_expression_transform
	| DIR string					#dir_string_transform
	| PATH general_expression		#path_general_expression_transform
	| PATH string					#path_string_transform
	| VAR? string					#var_transform
	;
	
transform_where_expression
	: WHERE general_expression
	;
	
transform_action_expression
	: PLUS OpenParen FROM transform_pick_up_expression transform_where_expression? CloseParen MAKE transform_action_chain
	;

/*===============*/
/*=====INPUT=====*/
input_and_finally_expression
	:	PLUS OpenParen ANDFINALLY and_finally_check_condition+ CloseParen
	;

input_let_identifier
	:	LET identity=string BE? (A|THE|SOME)?
	;
	
check_condition_completion
	:	(presence=(NONEPRESENT|ANYPRESENT) OR)? filter=(ANY|ALL) condition=(TRUE | FALSE) general_expression		
	|	presence=(NONEPRESENT|ANYPRESENT)																			
	;
	
var_where
	: var_name=string where_expression?
	;
	
and_finally_check_condition
	: FOR OpenParen	(var_where) (Comma var_where)* CloseParen	(THAT|WHICH)?	action=(FAILS | SUCCEEDS) (ON|WHEN) check_condition_completion
	;
	
check_condition
	// syntactic-sugar
	: (THAT|WHICH)?	action=(FAILS | SUCCEEDS) (ON|WHEN) check_condition_completion
	;
	
where_expression
	: WHERE general_expression
	;

dir_expression
	:	OpenParen	input_let_identifier?	(FILES IN)?	(DIR|DIR2)	(path_gen=general_expression|path_str=string)	where_expression?	check_condition? CloseParen	 
	;
	
path_expression
	:	OpenParen	input_let_identifier?	PATH	(path_gen=general_expression|path_str=string)	check_condition?	CloseParen
	;

input_expression
	:	PLUS (dir_expression | path_expression)
	;