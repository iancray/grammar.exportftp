﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using Antlr4.Runtime;
using DocuStream.Grammar.ExportFTP;
using DocuStream.Grammar.ExportFTP.General;
using DocuStream.Grammar.ExportFTP.ScriptCompilation;
using DocuStream.ProcessingFramework.Application;
using DocuStream.ProcessingFramework.Utilities;
using Ionic.Zip;
using Ionic.Zlib;
using Microsoft.CSharp;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InputExpressionTest
{

    [TestClass]
    public class UnitTest2
    {
        IEnvironmentState ini;
        CSharpScriptVisitor cssv;
        [TestInitialize]
        public void InitializeTestEnvironment()
        {
            ini = IniEnvironmentState.ReadIni("ExportFTP", "DocuStream");
            cssv = new CSharpScriptVisitor();
        }

        [TestMethod]
        public void TestSimple_GeneralExpressionIsType_Task_MethodInfo()
        {
            (var tok, var parser) = GetParserForFile("FullExpressionAlphaZulu.txt");
            cssv.TokenStream = tok;
            var code = parser.compileUnit().Accept(cssv);

            CSharpCodeProvider provider = new CSharpCodeProvider(new Dictionary<String, String> { { "CompilerVersion", "v4.0" } });
            CompilerParameters parameters = new CompilerParameters(
                new[]
                {
                    "System.dll",
                    "System.io.dll",
                    typeof(ZipFile).Assembly.Location,
                    typeof(CompressionLevel).Assembly.Location,
                    typeof(EFKit).Assembly.Location,
                    typeof(Image).Assembly.Location ,
                    typeof(IEnvironmentState).Assembly.Location,
                    typeof(IExportFTPProcessContext).Assembly.Location
                },
                "ExportFTP.DynamicCompiled.DLL",
                true
            );
            parameters.ReferencedAssemblies.Add(@"mscorlib.dll");
            parameters.ReferencedAssemblies.Add(@"System.Core.dll");

            CompilerResults results = provider.CompileAssemblyFromSource(parameters, (string)code);

            //_compiledType = results.CompiledAssembly.GetType("ExportFTP.DynamicallyCompiled.Functions");
            string basePath = results.TempFiles.BasePath;
            var fileEnding = basePath.Split(new[] { "Temp\\" }, StringSplitOptions.None)[1] + ".0.cs";

            File.WriteAllText(@"C:\Users\ian\" + fileEnding, (string)code);
        }

        (CommonTokenStream tok, ExportFTPParser parser) GetParserForFile(string fileName)
        {
            var _inputStream = new AntlrInputStream(File.Open(fileName, FileMode.Open));
            ExportFTPLexer _lexer = new ExportFTPLexer(_inputStream);
            CommonTokenStream _tok = new CommonTokenStream(_lexer);
            var _parser = new ExportFTPParser(_tok);

            return (_tok, _parser);
        }
    }
}

