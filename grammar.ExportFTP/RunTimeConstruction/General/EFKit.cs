﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using DocuStream.ProcessingFramework.Application;
using DocuStream.ProcessingFramework.Data;
using DocuStream.ProcessingFramework.Utilities;

namespace DocuStream.Grammar.ExportFTP.General
{
    public class EFKit
    {
        public Image Image                        { get; set; }
        public DateTimeTool DateTimeTool          { get; set; }
        public Regex Regex                        { get; set; }
        public FileInfo FileInfo                  { get; set; }
        public CounterTool CounterTool            { get; set; }
        public IEnvironmentState State            { get; set; }
        public ISet<EFFile> AllFiles              { get; set; }
        public ISet<EFFile> Output                { get; set; }
        public IBatchEntryProvider Provider       { get; set; }
        public Exception Exception                { get; set; }
        public Dictionary<string, string> Strings { get; set; }
        public Dictionary<string, object> Objects { get; set; }
        public List<IBatchEntry> SelectedBatches  { get; set; }

        public EFKit(IEnvironmentState state, IBatchEntryProvider provider)
        {
            this.State = state ?? throw new NullReferenceException("EFKit constructor expects a non-null IEnvironmentState input, but encountered state == null!");
            this.DateTimeTool = new DateTimeTool();
            this.CounterTool = new CounterTool(state["DailyCountersPath"]);
            this.Regex = new Regex("");
            this.Strings = new Dictionary<string, string>();
            this.Provider = provider;
            this.SelectedBatches = new List<IBatchEntry>();
        }
    }
}
