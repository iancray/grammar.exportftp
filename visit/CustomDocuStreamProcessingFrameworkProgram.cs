﻿using System;
using DocuStream.Grammar.ExportFTP.ScriptCompilation;
using DocuStream.ProcessingFramework.Application;
using DocuStream.ProcessingFramework.Data;
using DocuStream.ProcessingFramework.Reporting;
using DocuStream.ProcessingFramework.Utilities;
using visit;

public class CustomDocuStreamProcessingFrameworkProgram : IStartable
{
    private IEnvironmentState _state;
    private IReporter _reporter;
    private IExportFTPProcessContext _context;
    private Workflow _workflow;
    private bool _polling;

    /// <summary>
    /// A component for providing
    /// </summary>
    public IReporter Reporter { get => _reporter; set => _reporter = value; }

    public CustomDocuStreamProcessingFrameworkProgram(IEnvironmentState state, IExportFTPProcessContext context, IBatchEntryProvider batchEntryProvider, string moduleName, EntityConstructorFactory constructory = null)
    {
        var reporter = MultiChannelReporter.Default;
        this._state = state;
        this._reporter = reporter;
        this._context = context;
        this._workflow = new CustomWorkflow(context.Transform, state, batchEntryProvider, context, moduleName, reporter, constructory);
    }

    public virtual void Start()
    {
        Main();
    }

    public virtual void Main()
    {
        _context.InitializeContext();
        while (_polling)
        {
            try
            {
                _workflow.Process();
            }
            catch (Exception e)
            {
                break;
            }
        }
    }

    public void SetContinuePolling(bool polling)
    {
        this._polling = polling;
    }
}