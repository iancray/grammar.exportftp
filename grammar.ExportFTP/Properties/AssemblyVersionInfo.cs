﻿using System.Reflection;

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("2.0.0.2")]
[assembly: AssemblyFileVersion("2.0.0.2")]



// -- v2.0.0.0 - 04/15/2018 --  Update/ [cleanup] callback action implemented.
// -- v2.0.0.1 - 05/01/2018 --  Update/ XgAutoCompile now adds code to test validity of various SFTP connections that exist in the generated XgPlugin dll.
//                              Update/ In addition to that, the IExportFTPProcessContext interface has been updated to expose a method CheckAllSftpConnections(), which is where the validity of connections gets tested.
//                                      CustomWorkflow calls CheckAllSftpConnections() when the start function begins. If CheckAllSftpConnections fails, the Start function completes and an ERROR message is raised.
// -- v2.0.0.2 - 05/08/2018 --  Update/ Failed-to-connect log information improved. In the previous version, function names, not their call-resolutions, were being used in log file messages. This repair has been
//                                      implemented in the CSharpScripttVisitor.cs file.