﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocuStream.ProcessingFramework.Application;
using DocuStream.ProcessingFramework.Data;
using DocuStream.ProcessingFramework.Utilities;
using processing.ExportFTP;

namespace DocuStream.Processing.ExportFTP
{
    public class WorkflowListener : DocuStreamProcessingFrameworkProgram
    {
        private bool              _polling;
        private IEnvironmentState _state;
        private Reporter          _reporter;
        private ExportFTPContext  _context;
        private Workflow          _workflow;

        public WorkflowListener(Reporter reporter, IEnvironmentState state, ExportFTPContext context, IBatchEntryProvider batchEntryProvider, ErrLogger logger, string moduleName, EntityConstructorFactory constructory = null) : base(reporter, state, context, batchEntryProvider, logger, "ExportFTP")
        {
            this._state    = state;
            this._reporter = reporter;
            this._context  = context;
            this._workflow = new Workflow(reporter, state, batchEntryProvider, context, logger, moduleName, constructory);
        }

        public override void Main()
        {
            while (_polling)
            {
                try
                {
                    this._context.InitializeContext();
                    _workflow.Process();
                }
                catch (Exception e)
                {
                    _workflow.AddLogObject(new LogObject(e.Message, LogObjectType.ERROR));
                    break;
                }
            }
        }
    }
}
