﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocuStream.Grammar.ExportFTP.ScriptCompilation.FormattingComponents.component.cs
{
    public class SftpFunctionComponent
    {
        public string SftpClientSetup { get; set; }
        public string Destination { get; set; }
    }
}
