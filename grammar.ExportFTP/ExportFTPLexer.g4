﻿lexer grammar ExportFTPLexer;

/*
 * Lexer Rules
 */
										
WHERE										: 'where';
MAKE										: 'make' ;
FROM										: 'from' ;
VAR											: 'var' ;
LET											: 'let' ;
DOT											: '.';
BE											: 'be' ;
DIR											: 'dir' ;
DIR2										: 'directory' ;
PATH										: 'path' ;
INPUT										: 'input' ;
FAILS										: 'fails' ;
SUCCEEDS									: 'succeeds' ;
ON											: 'on' ;
WHEN										: 'when' ;
ANY											: 'any' ;
ALL											: 'all' ;
TRUE										: 'true' ;
FALSE										: 'false' ;
PLUS										: '+' ;
TRANSFORM									: 'transform' ;
THAT										: 'which' ;
WHICH										: 'that' ;
A											: 'a' ; // syntactic sugar.
THE											: 'the' ;
SOME										: 'some' ;
FILES										: 'files';
IN											: 'in';
ANDFINALLY									: 'and-finally-check' ;
OR 											: 'or' ;
NONEPRESENT									: 'none-present' ;
ANYPRESENT									: 'any-present' ;
FOR											: 'for' ;
CUSTOMER									: 'customer' ;
EVENTS										: 'events' ;
AS											: '>>' 
											;
 
//STRING										: (QuotationCharacter | DoubleQuotationCharacter) StringElement* (QuotationCharacter | DoubleQuotationCharacter);	
COMMENT										: ( ('#' | '//' | '--' ) .*? NewLineCharacter) -> channel(HIDDEN);
WhiteSpace									: [ ] -> channel(HIDDEN);
Tab											: [\t] -> channel(HIDDEN);
NewLineCharacter							: ('\r' '\n'?|'\n') -> channel(HIDDEN);
Colon										: ':';
OpenParen									: '(';
CloseParen									: ')';
DollarSign									: '$';
Comma	 									: ',';
CleanUpAction								: '[cleanup]' ;

					 
JustText									: [a-zA-Z]+ ;

IdentifierOrKeyword
	: IdentifierStartCharacter IdentifierPartCharacter*
	;

TRANSFORM_ACTION_MODE__START 				: '[' 
												-> pushMode(TRANSFORM_ACTION_MODE)
												
											; 

STRING_MODE__START							: '"' 
												-> pushMode(STRING_MODE)
												
											;

CSHARP_MODE__START							: '`' 
												-> pushMode(CSHARP_MODE)
												
											;
											
TRANSFORM_CONTINUATION_ARROW				: '->'
											;
											
											
fragment IdentifierStartCharacter
	: LetterCharacter
	| '_'
	;

fragment IdentifierPartCharacter
	: LetterCharacter
	| DigitCharacter
	;

fragment LetterCharacter
	: 'a'..'z' 
	| 'A'..'Z'
	;
	
fragment DigitCharacter
	: '0'..'9'
	;
	
fragment StringElement
   	: '\u0020' | '\u0021' | '\u0023' .. '\u007F' | CharEscapeSeq
   	;

fragment CharEscapeSeq
   	: '\\' ('b' | 't' | 'n' | 'f' | 'r' | '\\')
   	;
  
											
mode TRANSFORM_ACTION_MODE;
	TxWS					 	: [ \t\r\n]+ -> skip; // skip spaces, tabs, newlines
   	
	TRANSFORM_ACTION_TEXT		: [a-zA-Z-]+
								;
	TRANSFORM_ACTION_COLON		: ':'
								;

	TRANSFORM_ACTION_END		: ']'
									-> popMode
								;
								
	TRANSFORM_ACTION_MODE__AS   : '>>'
								;
								
	TRANSFORM_ACTION_MODE__CSHARP_START
								: '`'
									-> pushMode(CSHARP_MODE)
									
								;
								
	TRANSFORM_ACTION_MODE__STRING_START
								: '"'
									-> pushMode(STRING_MODE)
									
								;

mode STRING_MODE;
	StrWS : [ \t\r\n]+ -> skip; // skip spaces, tabs, newlines
						
	STRING_MODE__END 	: '"'  -> popMode
						;
						
	STRING_ANY	 		: ~["]+ ;
	
mode CSHARP_MODE;
	CshWS : [ \t\r\n]+ -> skip; // skip spaces, tabs, newlines
   	
	CSHARP_ANY			: ~[`]+ ;
	
	CSHARP_MODE__END	: '`'
							-> popMode
						;