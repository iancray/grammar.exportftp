﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocuStream.ProcessingFramework.Reporting;

namespace XgAutoCompileTests.CustomObservers
{
    public class MessageInterceptorObserver : IObserver<Message>
    {
        public List<string> InterceptedMessages { get; }

        public MessageInterceptorObserver()
        {
            InterceptedMessages = new List<string>();
        }

        public void OnCompleted()
        {
            throw new NotImplementedException();
        }

        public void OnError(Exception error)
        {
            throw new NotImplementedException();
        }

        public void OnNext(Message value)
        {
            InterceptedMessages.Add(value.Content);
        }
    }
}
