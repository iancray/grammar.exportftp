﻿using System;
using System.Collections;
using System.IO;
using Renci.SshNet;

namespace DocuStream.Grammar.ExportFTP.General.Transform.Utilities
{
    // v1.5.5 -- added in order to support multiple SFTP libraries.
    // we were facing some issues with the older SharpSsh library, since its from ~2007
    // as its last date of support.
    // Simple auto-refactor operation (based on original SFTPLib class) 
    // performed to create this interface.

    /// <summary>
    /// Interface for interaction with SFTP. Based on SFTPLib class.
    /// </summary>
    public interface ISFTPLib
    {
        bool ConnectSFTP();
        bool DeleteFile(string sftpPath, string strFile);
        void DisconnectSFTP();
        long GetFileSize(string strLocalPath, string strFile);
        bool IsSFTPConnected();
        ArrayList ListFiles(string sftpPath);
        void SFTPDownload(string sftpPath, string strFile, string strlocalpath);
        void SFTPUpload(string fromFilepath, string sftpPath, string strFile);
        bool Exists(string strFilePath, string strFile);
    }

    // v1.5.5 -- added to support HealthLinkSM in AutoFTP, valuable solution to be ported over to ExportFTP.
    //  SharpSsh library (which class SFTPLib is based on) was introducing 
    //  'Algorithm negotiation fail' exceptions, likely because SharpSsh
    //  did not support AES-256 algorithm. SSH.NET is the underlying library
    //  behind SSH_NET_SFTPLib. While SSH.NET is inspired by SharpSsh, it offers
    //  the benefit of support for more connection algorithms, and the promise of future
    //  API maintenance/support.  
    //  In the future if any 'Algorithm negotiation fail' errors occur for clients,
    //  Simply add the SFTPLibrary= to the ini and set it to SSH.NET. This will likely
    //  remedy most errors.

    /// <summary>
    /// SFTPLib implementation based on SSH.NET library, because the use of SharpSsh has been failing recently (dated 09/14/2017)
    /// </summary>
    public class SSH_NET_SFTPLib : ISFTPLib
    {

        private ConnectionInfo _connectionInfo;
        private SftpClient _client;

        public SSH_NET_SFTPLib(string host, string username, string password, int port = 22)
        {
            this._connectionInfo =
                new ConnectionInfo(
                    host,
                    username,
                    new PasswordAuthenticationMethod(username, password)
                );
            this._client = new SftpClient(this._connectionInfo);
        }

        public bool IsSFTPConnected()
        {
            return this._client.IsConnected;
        }

        public bool ConnectSFTP()
        {
            try
            {
                this._client.Connect();
                if (!this._client.IsConnected)
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                //clsDocuProgram.Log(string.Format("SFTP Connection Error Message: {0}", ex.Message));
                return false;
            }

            return true;
        }

        public ArrayList ListFiles(string sftpPath)
        {
            ArrayList list = new ArrayList();
            foreach (var file in this._client.ListDirectory(sftpPath))
            {
                list.Add(file.Name);
            }
            return list;
        }

        public long GetFileSize(string strLocalPath, string strFile)
        {
            long filesize = -1;

            FileInfo fi = new FileInfo(Path.Combine(strLocalPath, strFile));

            filesize = fi.Length;

            return filesize;
        }

        public void SFTPDownload(string sftpPath, string strFile, string strlocalpath)
        {
            //System.Windows.Forms.Application.DoEvents();
            if (this._client.IsConnected)
            {
                using (FileStream outStream = File.OpenWrite(Path.Combine(strlocalpath, strFile)))
                {
                    this._client.DownloadFile(Path.Combine(sftpPath, strFile), outStream);
                }
            }
        }

        public void SFTPUpload(string fromFilepath, string sftpPath, string strFile)
        {
            if (this._client.IsConnected)
            {
                using (FileStream inStream = File.OpenRead(Path.Combine(fromFilepath, strFile)))
                {
                    this._client.UploadFile(inStream, Path.Combine(sftpPath, strFile));
                }
            }
        }

        public void DisconnectSFTP()
        {
            if (this._client.IsConnected)
            {
                this._client.Disconnect();
            }
        }

        public bool DeleteFile(string sftpPath, string strFile)
        {
            try
            {
                if (this._client.IsConnected)
                {
                    this._client.DeleteFile(Path.Combine(sftpPath, strFile));
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool Exists(string strFilePath, string strFile)
        {
            return this._client.Exists(Path.Combine(strFilePath, strFile));
        }
    }
}
