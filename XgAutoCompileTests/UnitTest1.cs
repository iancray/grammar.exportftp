﻿using System;
using System.IO;
using System.Reflection;
using DocuStream.Grammar.ExportFTP.General;
using DocuStream.Grammar.ExportFTP.ScriptCompilation;
using DocuStream.ProcessingFramework.Reporting;
using DocuStream.ProcessingFramework.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using XgAutoCompileTests.CustomObservers;

namespace XgAutoCompileTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void XgPluginShouldFailBecauseOfBadFingerprint()
        {
            string testId = "Test1";

            // run xg-auto-compile
            string GetTestFile(string fileName) => Path.Combine("TestResources", fileName);
            if (XgAutoCompile.Program.Main(new string[] { GetTestFile($"{testId}.xg") }) == 0)
            {
                IniEnvironmentState state;
                try
                {
                    state = new IniEnvironmentState(GetTestFile(Path.Combine("Inis", $"{testId}.ini")), "ExportFTP", true);
                }
                catch (Exception exception)
                {
                    Assert.Fail("Could not load Test INI!");
                    return;
                }

                var assembly = Assembly.LoadFrom(GetTestFile($"ExportFTP.DynamicCompiled.{testId}.DLL"));

                var efk = new EFKit(state, null);
                var reporter = new MultiChannelReporterFactory().CreateMultiChannelReporterWithLogger(state["LogFilePath"]);

                var type = assembly.GetType("ExportFTP.ProcessContext.ExportFTPProcessContext");
                var ctor = type
                    .GetConstructor(new Type[] { typeof(EFKit), typeof(IReporter) });

                var invokation = ctor.Invoke(new object[] { efk, reporter });
                var procCtx = invokation as IExportFTPProcessContext;

                Assert.IsFalse(procCtx.CheckAllSftpConnections());
            }
            else
            {
                Assert.Fail("Failed to compile Test1.xg.");
            }
        }

        [TestMethod]
        public void XgPluginShouldFailWithTwoERRORMessagesRaised()
        {
            string testId = "Test4";

            var simpleChannelCounter = new SimpleChannelCallCounterObserver();

            // run xg-auto-compile
            string GetTestFile(string fileName) => Path.Combine("TestResources", fileName);
            if (XgAutoCompile.Program.Main(new string[] { GetTestFile($"{testId}.xg") }) == 0)
            {
                IniEnvironmentState state;
                try
                {
                    state = new IniEnvironmentState(GetTestFile(Path.Combine("Inis", $"{testId}.ini")), "ExportFTP", true);
                }
                catch (Exception exception)
                {
                    Assert.Fail("Could not load Test INI!");
                    return;
                }

                var assembly = Assembly.LoadFrom(GetTestFile($"ExportFTP.DynamicCompiled.{testId}.DLL"));

                var efk = new EFKit(state, null);
                var reporter = new MultiChannelReporterFactory().CreateMultiChannelReporterWithLogger(state["LogFilePath"]);

                reporter.Subscribe(simpleChannelCounter, false, "ERROR");

                var type = assembly.GetType("ExportFTP.ProcessContext.ExportFTPProcessContext");
                var ctor = type
                    .GetConstructor(new Type[] { typeof(EFKit), typeof(IReporter) });

                var invokation = ctor.Invoke(new object[] { efk, reporter });
                var procCtx = invokation as IExportFTPProcessContext;

                // should have failed twice!
                Assert.IsFalse(procCtx.CheckAllSftpConnections());
                // should be two because we have two failing SftpConnection checks in our xg file.
                Assert.IsTrue(simpleChannelCounter.Count == 2);
            }
            else
            {
                Assert.Fail("Failed to compile Test1.xg.");
            }
        }

        [TestMethod]
        public void XgPluginShouldSucceedBecauseOfKnownSFTP_AND_TakeAnyFingerprint()
        {
            string testId = "Test2";

            // run xg-auto-compile
            string GetTestFile(string fileName) => Path.Combine("TestResources", fileName);
            if (XgAutoCompile.Program.Main(new string[] { GetTestFile($"{testId}.xg") }) == 0)
            {
                IniEnvironmentState state;
                try
                {
                    state = new IniEnvironmentState(GetTestFile(Path.Combine("Inis", $"{testId}.ini")), "ExportFTP", true);
                }
                catch (Exception exception)
                {
                    Assert.Fail("Could not load Test INI!");
                    return;
                }

                var assembly = Assembly.LoadFrom(GetTestFile($"ExportFTP.DynamicCompiled.{testId}.DLL"));

                var efk = new EFKit(state, null);
                var reporter = new MultiChannelReporterFactory().CreateMultiChannelReporterWithLogger(state["LogFilePath"]);

                var type = assembly.GetType("ExportFTP.ProcessContext.ExportFTPProcessContext");
                var ctor = type
                    .GetConstructor(new Type[] { typeof(EFKit), typeof(IReporter) });

                var invokation = ctor.Invoke(new object[] { efk, reporter });
                var procCtx = invokation as IExportFTPProcessContext;

                Assert.IsTrue(procCtx.CheckAllSftpConnections());
            }
            else
            {
                Assert.Fail("Failed to compile Test1.xg.");
            }
        }

        [TestMethod]
        public void XgPluginShouldSucceedBecauseGoodFingerprint()
        {
            string testId = "Test3";

            // run xg-auto-compile
            string GetTestFile(string fileName) => Path.Combine("TestResources", fileName);
            if (XgAutoCompile.Program.Main(new string[] { GetTestFile($"{testId}.xg") }) == 0)
            {
                IniEnvironmentState state;
                try
                {
                    state = new IniEnvironmentState(GetTestFile(Path.Combine("Inis", $"{testId}.ini")), "ExportFTP", true);
                }
                catch (Exception exception)
                {
                    Assert.Fail("Could not load Test INI!");
                    return;
                }

                var assembly = Assembly.LoadFrom(GetTestFile($"ExportFTP.DynamicCompiled.{testId}.DLL"));

                var efk = new EFKit(state, null);
                var reporter = new MultiChannelReporterFactory().CreateMultiChannelReporterWithLogger(state["LogFilePath"]);

                var type = assembly.GetType("ExportFTP.ProcessContext.ExportFTPProcessContext");
                var ctor = type
                    .GetConstructor(new Type[] { typeof(EFKit), typeof(IReporter) });

                var invokation = ctor.Invoke(new object[] { efk, reporter });
                var procCtx = invokation as IExportFTPProcessContext;

                Assert.IsTrue(procCtx.CheckAllSftpConnections());
            }
            else
            {
                Assert.Fail("Failed to compile Test1.xg.");
            }
        }

        [TestMethod]
        public void XgPluginShouldPrintCannotConnectWithDescriptiveReason()
        {
            // The purpose of this test is to ensure that the compiler builds an exception message for the SFTPConnectionPreCheck
            // That accounts for the fact that we may be getting SFTP-Connection data from a function rather than just a string,
            // Jira task EXFTP-82 saw this kind of behavior being violated.

            string testId = "XgPluginShouldPrintCannotConnectWithDescriptiveReason";

            // run xg-auto-compile
            string GetTestFile(string fileName) => Path.Combine("TestResources", fileName);
            if (XgAutoCompile.Program.Main(new string[] { GetTestFile($"{testId}.xg") }) == 0)
            {
                IniEnvironmentState state;
                try
                {
                    state = new IniEnvironmentState(GetTestFile(Path.Combine("Inis", $"{testId}.ini")), "ExportFTP", true);
                }
                catch (Exception exception)
                {
                    Assert.Fail("Could not load Test INI!");
                    return;
                }

                var assembly = Assembly.LoadFrom(GetTestFile($"ExportFTP.DynamicCompiled.{testId}.DLL"));

                var efk = new EFKit(state, null);
                var reporter = new MultiChannelReporterFactory().CreateMultiChannelReporterWithLogger(state["LogFilePath"]);

                var interceptor = new MessageInterceptorObserver();

                reporter.Subscribe(interceptor, false, "ERROR");

                var type = assembly.GetType("ExportFTP.ProcessContext.ExportFTPProcessContext");
                var ctor = type
                    .GetConstructor(new Type[] { typeof(EFKit), typeof(IReporter) });

                var invokation = ctor.Invoke(new object[] { efk, reporter });
                var procCtx = invokation as IExportFTPProcessContext;
                
                Assert.IsFalse(procCtx.CheckAllSftpConnections());
                Assert.IsTrue(interceptor.InterceptedMessages.Count == 1);
                Assert.IsTrue(interceptor.InterceptedMessages[0].Contains("failureCauseOne"));
                Assert.IsTrue(interceptor.InterceptedMessages[0].Contains("failureCauseTwo"));
            }
            else
            {
                Assert.Fail("Failed to compile Test1.xg.");
            }
        }
    }
}
