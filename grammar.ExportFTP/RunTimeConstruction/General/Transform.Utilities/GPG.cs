﻿using System;
using System.IO;

namespace DocuStream.Grammar.ExportFTP.General.Transform.Utilities
{
    /// <summary>
    /// To be used with the GnuPG command line utility. http://www.gnupg.org/
    /// </summary>
    public class GPG_DS : PGP_DS
    {
        public const int PGP_VERSION_DEFAULT = 6;

        public GPG_DS() : base() { }
        public GPG_DS(string strPGPLocation, string strPassPhrase) : base(strPGPLocation, strPassPhrase) { }
        public GPG_DS(string strPGPLocation, string strPassPhrase, string strToUserName, string strMyUserName)
            : base(strPGPLocation, strPassPhrase, strToUserName, strMyUserName) { }
        public GPG_DS(string strPGPLocation, string strPassPhrase, string strToUserName, string strMyUserName, string strSecring, string strPubring)
            : base(strPGPLocation, strPassPhrase, strToUserName, strMyUserName, strSecring, strPubring) { }
        public GPG_DS(string strPGPLocation, string strPassPhrase, string strToUserName, string strMyUserName, string strSecring, string strPubring, int intWaitTime)
            : base(strPGPLocation, strPassPhrase, strToUserName, strMyUserName, strSecring, strPubring, intWaitTime) { }

        public override string Encrypt(string filename, bool useArmor)
        { return Encrypt(filename, useArmor, PGP_VERSION_DEFAULT); }
        public virtual string Encrypt(string filename, bool useArmor, int pgpVersion)
        {
            string strArgs = "";

            if (!File.Exists(filename))
                throw new Exception("Could not locate file: " + filename);

            // there could be multiple recipients that we are encrypting this file to
            string[] arrRecipients = _strToUserName.Split(new char[] { '|' });
            string strRecipients = "";
            foreach (string s in arrRecipients)
                strRecipients += "-r \"" + s + "\" ";

            // ex. gpg -r "Derrick Dela Cruz" --keyring "C:\Documents and Settings\DerrickD\My Documents\PGP\pubring.pkr" 
            //	--secret-keyring "C:\Documents and Settings\DerrickD\My Documents\PGP\secring.skr" --trust-model always 
            //	--pgp8 --passphrase docu101stream -se F123456.Mdb

            strArgs = string.Format("{0} -u \"{1}\" --passphrase {2} --trust-model always --yes ",
                strRecipients, _strMyUserName, _strPassPhrase);

            // if there is a configuration file in the same directory as the exe, load it
            string strConfigFile = Path.Combine(new FileInfo(_strPgpDirectory).DirectoryName, "gpg.conf");
            if (File.Exists(strConfigFile))
                strArgs += "--options \"" + strConfigFile + "\" ";

            // PGP produces different outputs for binary and plain text files
            if (useArmor)
                strArgs += "-a "; // ascii armor (for text files?)
            if (_strSecring != "")
                strArgs += "--secret-keyring \"" + _strSecring + "\" ";
            if (_strPubring != "")
                strArgs += "--keyring \"" + _strPubring + "\" ";

            // make as compliant as possible to pgp version
            if (pgpVersion > 0)
                strArgs += "--pgp" + pgpVersion.ToString() + " ";

            // sign and encrypt file needs to be the last argument
            strArgs += "-se \"" + filename + "\"";

            _proc.StartInfo.FileName = _strPgpDirectory;
            _proc.StartInfo.Arguments = strArgs;
            _proc.StartInfo.UseShellExecute = false;
            _proc.StartInfo.RedirectStandardOutput = true;
            _proc.StartInfo.RedirectStandardError = true;
            _proc.StartInfo.CreateNoWindow = true;

            _proc.Start();

            if (_intWaitTime == -1)
                _proc.WaitForExit(); // can wait for infinity
            else
                _proc.WaitForExit(_intWaitTime);

            string strOutput = _proc.StandardOutput.ReadToEnd();
#pragma WARNING: MAYBE NOT GOOD
            //if (strOutput.Trim().Length > 0)
            //    Log("GPG Output: \n" + strOutput);

            string strErrors = _proc.StandardError.ReadToEnd();
#pragma WARNING: MAYBE NOT GOOD
            //if (strErrors.Trim().Length > 0)
            //    Log("GPG Output-Errors/Warnings: \r\n" + strErrors.Trim());

            if (_proc.HasExited)
            {
                int ab = _proc.ExitCode;
                if (ab != 0)
                    throw new Exception("-- GPG exit code: " + _proc.ExitCode);
                else if (useArmor)
                    return filename + ".asc";

                // rename from .gpg to .pgp
                try
                {
                    File.Move(filename + ".gpg", filename + ".pgp");
                }
                catch
                {
                    File.Delete(filename + ".pgp");
                    File.Move(filename + ".gpg", filename + ".pgp");
                }
                return filename + ".pgp";
            }

            return "";
        }

        public override string Decrypt(string filename)
        {
            if (!File.Exists(filename))
                throw new Exception("Could not locate file: " + filename);

            string strArgs = "";
            string strOutputFile = filename.Substring(0, filename.Length - 4);

            //ex. gpg --keyring "C:\Documents and Settings\DerrickD\My Documents\PGP\pubring.pkr" --secret-keyring 
            //	"C:\Documents and Settings\DerrickD\My Documents\PGP\secring.skr" --passphrase docu101stream --yes -o F123456.Mdb -d F123456.Mdb.pgp

            strArgs = "--passphrase " + _strPassPhrase + " --trust-model always --yes ";

            // if there is a configuration file in the same directory as the exe, load it (you can load idea dll from conf file)
            string strConfigFile = Path.Combine(new FileInfo(_strPgpDirectory).DirectoryName, "gpg.conf");
            if (File.Exists(strConfigFile))
                strArgs += "--options \"" + strConfigFile + "\" ";

            if (_strSecring != "")
                strArgs += "--secret-keyring \"" + _strSecring + "\" ";
            if (_strPubring != "")
                strArgs += "--keyring \"" + _strPubring + "\" ";

            // file to decrypt needs to be the last argument
            strArgs += string.Format("-o \"{0}\" -d \"{1}\"", strOutputFile, filename);

            _proc.StartInfo.FileName = _strPgpDirectory;
            _proc.StartInfo.Arguments = strArgs;
            _proc.StartInfo.UseShellExecute = false;
            _proc.StartInfo.RedirectStandardOutput = true;
            _proc.StartInfo.RedirectStandardError = true;
            _proc.StartInfo.CreateNoWindow = true;

            _proc.Start();

            if (_intWaitTime == -1)
                _proc.WaitForExit();
            else
                _proc.WaitForExit(_intWaitTime);

            string strOutput = _proc.StandardOutput.ReadToEnd();
#pragma warning maybe not good
            //if (strOutput.Trim().Length > 0)
            //    Log("GPG Output: \n" + strOutput);

            string strErrors = _proc.StandardError.ReadToEnd();
#pragma warning maybe not good
            //if (strErrors.Trim().Length > 0)
            //    Log("GPG Output-Errors/Warnings: \r\n" + strErrors.Trim());

            if (_proc.HasExited)
            {
                int ab = _proc.ExitCode;
                if (ab != 0)
                    throw new Exception("-- GPG exit code: " + _proc.ExitCode);
                else
                    return strOutputFile;
            }

            return "";
        }

        public bool Import(string strKeyring)
        {
            string strArgs = "";

            strArgs = string.Format("--import \"" + strKeyring + "\"");

            _proc.StartInfo.FileName = _strPgpDirectory;
            _proc.StartInfo.Arguments = strArgs;
            _proc.Start();

            if (_intWaitTime == -1)
                _proc.WaitForExit();
            else
                _proc.WaitForExit(_intWaitTime);

            if (_proc.HasExited)
            {
                int ab = _proc.ExitCode;
                if (ab != 0)
                    throw new Exception("-- GPG exit code: " + _proc.ExitCode);
                else
                    return true;
            }

            return false;
        }

    }

    /// <summary>
	/// To be used with the PGP command line utility (From PGP6. PGP8 also has a command line utility but it is not free).
	/// </summary>
	public class PGP_DS
    {
        protected string _strPgpDirectory = "";
        protected string _strToUserName = "";
        protected string _strMyUserName = "";
        protected string _strPassPhrase = "";
        protected string _strSecring = "";
        protected string _strPubring = "";
        protected int _intWaitTime = -1;

        protected System.Diagnostics.Process _proc = new System.Diagnostics.Process();

        // constructors
        public PGP_DS() { }
        public PGP_DS(string strPGPLocation, string strPassPhrase) : this(strPGPLocation, strPassPhrase, "", "") { }
        public PGP_DS(string strPGPLocation, string strPassPhrase, string strToUserName, string strMyUserName)
            : this(strPGPLocation, strPassPhrase, strToUserName, strMyUserName, "", "") { }
        public PGP_DS(string strPGPLocation, string strPassPhrase, string strToUserName, string strMyUserName, string strSecring, string strPubring)
            : this(strPGPLocation, strPassPhrase, strToUserName, strMyUserName, strSecring, strPubring, -1) { }
        public PGP_DS(string strPGPLocation, string strPassPhrase, string strToUserName, string strMyUserName, string strSecring, string strPubring, int intWaitTime)
        {
            _strPgpDirectory = strPGPLocation;
            _strPassPhrase = strPassPhrase;
            _strToUserName = strToUserName;
            _strMyUserName = strMyUserName;
            _strSecring = strSecring;
            _strPubring = strPubring;
            _intWaitTime = intWaitTime;
        }

        public virtual string Encrypt(string filename)
        {
            return Encrypt(filename, false);
        }
        public virtual string Encrypt(string filename, bool useArmor)
        {
            string outputfilename = filename;
            string strArgs = "";

            if (!File.Exists(filename))
                throw new Exception("Could not locate file: " + filename);

            strArgs = string.Format("+force -es \"{0}\" \"{1}\" -u \"{2}\" -z {3} ",
                filename, _strToUserName, _strMyUserName, _strPassPhrase);

            // PGP can use ascii armor for text files
            if (useArmor)
                strArgs += "-a ";
            if (_strSecring != "")
                strArgs += "+secring=\"" + _strSecring + "\" ";
            if (_strPubring != "")
                strArgs += "+pubring=\"" + _strPubring + "\" ";

            _proc.StartInfo.FileName = _strPgpDirectory;
            _proc.StartInfo.Arguments = strArgs;

            _proc.Start();
            if (_intWaitTime == -1)
                _proc.WaitForExit(); // can wait for infinity
            else
                _proc.WaitForExit(_intWaitTime);

            if (_proc.HasExited)
            {
                int ab = _proc.ExitCode;
                if (ab != 0)
                    throw new Exception("-- PGP exit code: " + _proc.ExitCode);
                if (useArmor)
                    return outputfilename + ".asc";
                return outputfilename + ".pgp";
            }

            return "";
        }

        public virtual string Decrypt(string filename)
        {
            string outputfilename = filename;
            string strArgs = "";

            if (!File.Exists(filename))
                throw new Exception("Could not locate file: " + filename);

            strArgs = string.Format("+force \"{0}\" -z {1} ", filename, _strPassPhrase);

            if (_strSecring != "")
                strArgs += "+secring=\"" + _strSecring + "\" ";

            _proc.StartInfo.FileName = _strPgpDirectory;
            _proc.StartInfo.Arguments = strArgs;
            _proc.Start();

            if (_intWaitTime == -1)
                _proc.WaitForExit();
            else
                _proc.WaitForExit(_intWaitTime);

            if (_proc.HasExited)
            {
                int ab = _proc.ExitCode;
                if (ab != 0)
                    throw new Exception("-- PGP exit code: " + _proc.ExitCode);
                else
                    return outputfilename.Substring(0, outputfilename.Length - 4);
            }

            return "";
        }
    }
}

