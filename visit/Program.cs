﻿using System;
using System.Linq;
using System.Reflection;
using DocuStream.Grammar.ExportFTP.General;
using DocuStream.Grammar.ExportFTP.ScriptCompilation;
using DocuStream.ProcessingFramework.Data;
using DocuStream.ProcessingFramework.Reporting;
using DocuStream.ProcessingFramework.Utilities;

namespace visit
{
    class Program
    {
        static void Main(string[] args)
        {
            //var assembly = Assembly.LoadFrom(args[0]);
            //var type = assembly.GetTypes().First(_ => _.FullName == "ExportFTP.DynamicallyCompiled.Functions");
            


            IniEnvironmentState iniEnvironmentState = IniEnvironmentState.ReadIni("ExportFTP", "DocuStream");
            EFKit efk = new EFKit(iniEnvironmentState);
            var reporter = MultiChannelReporter.Default;

            LogWriter logWriter = LogWriter.DefaultLogWriterFactory(iniEnvironmentState["LogFilePath"]);
            reporter.Subscribe(logWriter, true, "LOG", "ERROR");
            var assembly = Assembly
                .LoadFrom(iniEnvironmentState["TargetAssembly"]);
            
            var type = assembly.GetType("ExportFTP.ProcessContext.ExportFTPProcessContext");
            var ctor = type
                .GetConstructor(new Type[] { typeof(EFKit), typeof(IReporter) });

            var invokation = ctor.Invoke(new object[] { efk, reporter });
            var procCtx = invokation as IExportFTPProcessContext;

            var prog = new CustomDocuStreamProcessingFrameworkProgram(
                iniEnvironmentState,
                procCtx, 
                BatchEntryProvisioning.GetBatchEntryProvider(iniEnvironmentState, "ExportFTP", new string[] { }), 
                "ExportFTP"
            );
            prog.SetContinuePolling(true);
            prog.Main();
        }
    }
}